package sk.stuba.fei.stumee.model.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;

import sk.stuba.fei.stumee.model.Link;

@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
public class User implements Serializable {
    private int id;
    private String username;
    private String stuEmail;
    private String secondaryEmail;
    private boolean confirmed;
    private boolean deactivated;
    private String name;
    private String surname;
    private String description;
    private List<Link> links;

    public User() {
    }

    public User(int id, String username, String stuEmail, String secondaryEmail, boolean confirmed, boolean deactivated, String name, String surname, String description, List<Link> links) {
        this.id = id;
        this.username = username;
        this.stuEmail = stuEmail;
        this.secondaryEmail = secondaryEmail;
        this.confirmed = confirmed;
        this.deactivated = deactivated;
        this.name = name;
        this.surname = surname;
        this.description = description;
        this.links = links;
    }

    @SuppressWarnings("SimplifiableIfStatement")
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (getId() != user.getId()) return false;
        if (!getUsername().equals(user.getUsername())) return false;
        if (getName() != null ? !getName().equals(user.getName()) : user.getName() != null)
            return false;
        return getSurname() != null ? getSurname().equals(user.getSurname()) : user.getSurname() == null;

    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + getUsername().hashCode();
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getSurname() != null ? getSurname().hashCode() : 0);
        return result;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getStuEmail() {
        return stuEmail;
    }

    public void setStuEmail(String stuEmail) {
        this.stuEmail = stuEmail;
    }

    public String getSecondaryEmail() {
        return secondaryEmail;
    }

    public void setSecondaryEmail(String secondaryEmail) {
        this.secondaryEmail = secondaryEmail;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public boolean isDeactivated() {
        return deactivated;
    }

    public void setDeactivated(boolean deactivated) {
        this.deactivated = deactivated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", stuEmail='" + stuEmail + '\'' +
                ", secondaryEmail='" + secondaryEmail + '\'' +
                ", confirmed=" + confirmed +
                ", deactivated=" + deactivated +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", description='" + description + '\'' +
                ", links=" + links +
                '}';
    }
}
