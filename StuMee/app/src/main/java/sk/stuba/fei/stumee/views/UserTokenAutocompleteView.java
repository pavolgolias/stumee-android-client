package sk.stuba.fei.stumee.views;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tokenautocomplete.TokenCompleteTextView;

import sk.stuba.fei.stumee.R;
import sk.stuba.fei.stumee.model.user.User;

public class UserTokenAutocompleteView extends TokenCompleteTextView<User> {
    public UserTokenAutocompleteView(Context context) {
        super(context);
    }

    public UserTokenAutocompleteView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public UserTokenAutocompleteView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected View getViewForObject(User user) {
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        LinearLayout view = (LinearLayout) layoutInflater.inflate(R.layout.token_autocomplete_layout, (ViewGroup) UserTokenAutocompleteView.this.getParent(), false);

        String tokenString = user.getName() + " " + user.getSurname() + " (" + user.getUsername() + ")";
        ((TextView) view.findViewById(R.id.token_auto_layout_text)).setText(tokenString);

        return view;
    }

    @Override
    protected User defaultObject(String completionText) {
        return null;
    }
}
