package sk.stuba.fei.stumee.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import sk.stuba.fei.stumee.model.user.User;

@SuppressWarnings("BooleanMethodIsAlwaysInverted")
public class UserUtils {
    private static final String LOG_TAG = "UserUtils";
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String STU_EMAIL_SUFFIX = "@stuba.sk";

    public static boolean isStuEmailValid(String email) {
        return !email.isEmpty() && email.matches(EMAIL_PATTERN) && email.endsWith(STU_EMAIL_SUFFIX);
    }

    public static boolean isEmailValid(String email) {
        return !email.isEmpty() && email.matches(EMAIL_PATTERN);
    }

    public static boolean isPasswordValid(String password) {
        return !password.isEmpty();
    }

    public static boolean isNickNameValid(String nickName) {
        return !nickName.isEmpty() && nickName.length() > 3 && nickName.length() < 30;
    }

    public static String getNameFromWholeAisName(String wholeName) {
        String[] tokens = wholeName.split(",");
        String[] nameTokens = tokens[0].split(" ");

        if (nameTokens.length >= 1) {
            return nameTokens[1];
        } else {
            return nameTokens[0];
        }
    }

    public static String getSurnameFromWholeAisName(String wholeName) {
        String[] tokens = wholeName.split(",");
        String[] nameTokens = tokens[0].split(" ");
        return nameTokens[0];
    }

    public static void saveUserObjectToPrefs(Context context, User user) {
        String jsonString = getJsonFromUserObject(user);

        if (jsonString != null) {
            SharedPreferences.Editor editor = context.getSharedPreferences(Constants.USER_GROUP_PREFS_KEY, 0).edit();
            editor.putString(Constants.USER_JSON_PREFS_KEY, jsonString).apply();
        }
    }

    public static User loadUserObjectFromPrefs(Context context) {
        try {
            SharedPreferences preferences = context.getSharedPreferences(Constants.USER_GROUP_PREFS_KEY, 0);
            String jsonString = preferences.getString(Constants.USER_JSON_PREFS_KEY, "");

            if (!jsonString.isEmpty()) {
                return getUserObjectFromJson(jsonString);
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "Error while loading user object from Shared Preferences!" + e.toString());
        }

        return null;
    }

    public static void removeUserObjectFromPrefs(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(Constants.USER_GROUP_PREFS_KEY, 0).edit();
        editor.remove(Constants.USER_JSON_PREFS_KEY).apply();

        removeUserPictureFile(context);
    }

    public static void saveUserPictureToFile(Context context, byte[] input) {
        try {
            FileOutputStream fos = context.openFileOutput(Constants.USER_PICTURE_NAME, Context.MODE_PRIVATE);
            fos.write(input);
            fos.close();
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error while writing user picture to file. " + e);
        }
    }

    public static Bitmap loadUserPictureFromFile(Context context) {
        try {
            FileInputStream fis = context.openFileInput(Constants.USER_PICTURE_NAME);
            Bitmap bitmap = BitmapFactory.decodeStream(fis);
            fis.close();

            return bitmap;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void removeUserPictureFile(Context context) {
        context.deleteFile(Constants.USER_PICTURE_NAME);
    }

    public static String getJsonFromUserObject(User user) {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            return objectMapper.writeValueAsString(user);
        } catch (JsonProcessingException e) {
            Log.e(LOG_TAG, "Error while creating json String from object: " + e);
        }

        return null;
    }

    public static User getUserObjectFromJson(String jsonString) {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            return objectMapper.readValue(jsonString, User.class);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error while creating user object from json String: " + e);
        }

        return null;
    }
}
