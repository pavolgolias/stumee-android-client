package sk.stuba.fei.stumee.model.event.comments;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;
import java.util.List;

import sk.stuba.fei.stumee.model.user.User;
import sk.stuba.fei.stumee.model.Link;

@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Comment {
    private int id;
    private String content;
    private Date timestamp;
    private User author;
    private List<Link> links;

    public Comment() {
    }

    public Comment(int id, String content, Date timestamp, User author, List<Link> links) {
        this.id = id;
        this.content = content;
        this.timestamp = timestamp;
        this.author = author;
        this.links = links;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", timestamp=" + timestamp +
                ", author=" + author +
                ", links=" + links +
                '}';
    }
}
