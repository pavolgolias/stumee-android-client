package sk.stuba.fei.stumee.model.user.regAutocomplete;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegAutocompleteResponseContent {
    private String wholeName;
    private String department;
    private int id;

    public RegAutocompleteResponseContent() {
    }

    public RegAutocompleteResponseContent(String wholeName, String department, int id) {
        this.wholeName = wholeName;
        this.department = department;
        this.id = id;
    }

    public String getWholeName() {
        return wholeName;
    }

    public void setWholeName(String wholeName) {
        this.wholeName = wholeName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "RegAutocompleteResponseContent{" +
                "wholeName='" + wholeName + '\'' +
                ", department='" + department + '\'' +
                ", id=" + id +
                '}';
    }
}
