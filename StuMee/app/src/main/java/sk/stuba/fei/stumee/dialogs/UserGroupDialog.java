package sk.stuba.fei.stumee.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import java.util.List;

import sk.stuba.fei.stumee.R;
import sk.stuba.fei.stumee.model.user.User;

public class UserGroupDialog extends DialogFragment {
    private static final String DIALOG_TITLE = "title";
    private static final String DIALOG_CONTENT = "content";

    public static UserGroupDialog newInstance(String title, List<User> users) {
        UserGroupDialog userGroupDialog = new UserGroupDialog();
        Bundle args = new Bundle();

        args.putString(DIALOG_TITLE, title);
        args.putString(DIALOG_CONTENT, createNamesList(users));
        userGroupDialog.setArguments(args);

        return userGroupDialog;
    }

    private static String createNamesList(List<User> users) {
        StringBuilder builder = new StringBuilder();

        for (User user : users) {
            builder.append(user.getName()).append(" ").append(user.getSurname())
                    .append(" (").append(user.getUsername()).append(")")
                    .append("\n");
        }

        return builder.toString();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(getArguments().getString(DIALOG_TITLE))
                .setMessage(getArguments().getString(DIALOG_CONTENT))
                .setIcon(R.mipmap.ic_launcher)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        return builder.create();
    }
}
