package sk.stuba.fei.stumee;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import sk.stuba.fei.stumee.asyncTasks.AbstractApiAsyncTask;
import sk.stuba.fei.stumee.dialogs.UserGroupDialog;
import sk.stuba.fei.stumee.model.contents.EventCommentsContent;
import sk.stuba.fei.stumee.model.contents.UsersContent;
import sk.stuba.fei.stumee.model.event.Event;
import sk.stuba.fei.stumee.model.event.EventCreate;
import sk.stuba.fei.stumee.model.event.EventVisibility;
import sk.stuba.fei.stumee.model.event.comments.Comment;
import sk.stuba.fei.stumee.model.event.comments.CommentCreate;
import sk.stuba.fei.stumee.model.user.User;
import sk.stuba.fei.stumee.utils.AuthUtils;
import sk.stuba.fei.stumee.utils.Constants;
import sk.stuba.fei.stumee.utils.EventUtils;
import sk.stuba.fei.stumee.utils.UserUtils;
import sk.stuba.fei.stumee.utils.linking.LinkTypeCommon;
import sk.stuba.fei.stumee.utils.linking.LinkTypeEventUser;
import sk.stuba.fei.stumee.utils.linking.LinkUtils;
import sk.stuba.fei.stumee.utils.pagination.PaginationParam;
import sk.stuba.fei.stumee.utils.pagination.PaginationSortType;
import sk.stuba.fei.stumee.utils.pagination.PaginationUtils;

public class EventDetailFragment extends Fragment {
    public static final String ARGS_EVENT_ID_ARGUMENT = "stumee.eventId";
    public static final String ARGS_IS_TWO_PANE_MODE = "stumee.event.twoPane";
    public static final String ARGS_CURRENT_USER = "stumee.event.currentUser";
    private static final String LOG_TAG = "EventDetailFragment";

    private ProgressDialog progressDialog;
    private RecyclerView eventDetailRecyclerView;
    private Map<LinkTypeEventUser, List<User>> eventUserGroupsMap;
    private boolean isTwoPane = false;
    private User currentUser;

    public EventDetailFragment() {
        this.eventUserGroupsMap = new HashMap<>();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARGS_IS_TWO_PANE_MODE)) {
            isTwoPane = getArguments().getBoolean(ARGS_IS_TWO_PANE_MODE, false);
        }
        if (getArguments().containsKey(ARGS_EVENT_ID_ARGUMENT) && getArguments().containsKey(ARGS_CURRENT_USER)) {
            currentUser = UserUtils.getUserObjectFromJson(getArguments().getString(ARGS_CURRENT_USER));
            new LoadEventDetailAsyncTask(getArguments().getString(ARGS_EVENT_ID_ARGUMENT)).execute();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.event_detail, container, false);

        eventDetailRecyclerView = (RecyclerView) rootView.findViewById(R.id.event_detail_view_comments_recycler);
        setupRecyclerView(null, new ArrayList<Comment>());

        return rootView;
    }

    private void setCollapsingToolbarTitle(String title) {
        Activity activity = this.getActivity();
        CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.event_detail_collapsing_toolbar_layout);
        TextView appBarSecondTitle = (TextView) activity.findViewById(R.id.event_detail_collapsing_big_title);

        if (appBarLayout != null) {
            appBarLayout.setTitle(title);
            appBarLayout.setExpandedTitleColor(ContextCompat.getColor(getContext(), android.R.color.transparent));
        }

        if (appBarSecondTitle != null) {
            String secondTitleString = title.replaceAll("\n", " ");

            if (secondTitleString.length() > 85) {
                secondTitleString = secondTitleString.substring(0, 85) + "...";
            }

            appBarSecondTitle.setText(secondTitleString);
        }
    }

    private void setupRecyclerView(Event event, @NonNull List<Comment> comments) {
        if (eventDetailRecyclerView != null) {
            eventDetailRecyclerView.setAdapter(new EventDetailViewAdapter(event, comments));
        }
    }

    private void showAppropriateContentForUser(Event event) {
        LinkTypeEventUser userGroupLinkTypeEventUser = getCurrentUserGroup();
        TextView userGroupTextView;
        try {
            userGroupTextView = (TextView) eventDetailRecyclerView.getLayoutManager().findViewByPosition(0).findViewById(R.id.event_detail_view_user_group_text);
        } catch (NullPointerException e) {
            Log.e(LOG_TAG, "Error while getting reference of user group TextView. " + e.toString());
            return;
        }

        if (userGroupLinkTypeEventUser == null) {
            if (event.getVisibility().equals(EventVisibility.PUBLIC)) {
                setContentVisibilityForUser(true, true, false);         // no user group && public event == accept/decline
                userGroupTextView.setText(R.string.event_detail_view_users_text_1);
            }
        } else {
            if (userGroupLinkTypeEventUser.equals(LinkTypeEventUser.EVENT_AUTHORS)) {
                setContentVisibilityForUser(false, false, true);        // author of event == edit
                userGroupTextView.setText(R.string.event_detail_view_users_text_2);
            } else if (userGroupLinkTypeEventUser.equals(LinkTypeEventUser.EVENT_INVITED)) {
                setContentVisibilityForUser(true, true, false);        // invited == accept/decline
                userGroupTextView.setText(R.string.event_detail_view_users_text_3);
            } else if (userGroupLinkTypeEventUser.equals(LinkTypeEventUser.EVENT_NOT_GOING)) {
                setContentVisibilityForUser(true, false, false);        // not going == accept
                userGroupTextView.setText(R.string.event_detail_view_users_text_4);
            } else if (userGroupLinkTypeEventUser.equals(LinkTypeEventUser.EVENT_GOING)) {
                setContentVisibilityForUser(false, true, false);        // going == decline
                userGroupTextView.setText(R.string.event_detail_view_users_text_5);
            }
        }
    }

    private void setContentVisibilityForUser(boolean showAccept, boolean showDecline, boolean showEdit) {
        eventDetailRecyclerView.getLayoutManager().findViewByPosition(0).findViewById(R.id.event_detail_view_invitation_accept)
                .setVisibility(showAccept ? View.VISIBLE : View.GONE);
        eventDetailRecyclerView.getLayoutManager().findViewByPosition(0).findViewById(R.id.event_detail_view_invitation_decline)
                .setVisibility(showDecline ? View.VISIBLE : View.GONE);
        eventDetailRecyclerView.getLayoutManager().findViewByPosition(0).findViewById(R.id.event_detail_view_edit_button)
                .setVisibility(showEdit ? View.VISIBLE : View.GONE);
    }

    private LinkTypeEventUser getCurrentUserGroup() {
        if (currentUser == null) {
            return null;
        }

        for (LinkTypeEventUser linkTypeEventUser : this.eventUserGroupsMap.keySet()) {
            for (User user : this.eventUserGroupsMap.get(linkTypeEventUser)) {
                if (currentUser.getId() == user.getId()) {
                    return linkTypeEventUser;
                }
            }
        }

        return null;
    }

    private void moveUserToGroupByAcceptance(boolean isGoing) {
        TextView userGroupTextView = (TextView) eventDetailRecyclerView.getLayoutManager().findViewByPosition(0).findViewById(R.id.event_detail_view_user_group_text);
        List<User> goingUsers = eventUserGroupsMap.get(LinkTypeEventUser.EVENT_GOING);
        List<User> notGoingUsers = eventUserGroupsMap.get(LinkTypeEventUser.EVENT_NOT_GOING);

        if (currentUser == null || userGroupTextView == null || goingUsers == null || notGoingUsers == null) {
            return;
        }

        if (isGoing) {
            goingUsers.add(currentUser);
            notGoingUsers.remove(currentUser);
            userGroupTextView.setText(R.string.event_detail_view_users_text_5);
        } else {
            goingUsers.remove(currentUser);
            notGoingUsers.add(currentUser);
            userGroupTextView.setText(R.string.event_detail_view_users_text_4);
        }

        eventUserGroupsMap.put(LinkTypeEventUser.EVENT_GOING, goingUsers);
        eventUserGroupsMap.put(LinkTypeEventUser.EVENT_NOT_GOING, notGoingUsers);
    }

    private void showEditActivity(Event event) {
        EventCreate eventCreate = new EventCreate(event.getTitle(), event.getDescription(), event.getTimestamp(), event.getType(),
                event.getVisibility(), event.getLocation());
        Intent intent = new Intent(getActivity(), CreateEditEventActivity.class);
        intent.putExtra(CreateEditEventActivity.EDIT_EVENT_JSON, EventUtils.getJsonFromEventCreate(eventCreate));
        intent.putExtra(CreateEditEventActivity.EDIT_EVENT_ID, event.getId());
        startActivity(intent);
    }

    private void showUserDialogFragment(LinkTypeEventUser linkTypeEventUser) {
        String[] allTitles = getResources().getStringArray(R.array.event_detail_users_groups);
        List<User> users = this.eventUserGroupsMap.get(linkTypeEventUser);

        String dialogTitle = "";
        if (LinkTypeEventUser.EVENT_AUTHORS.equals(linkTypeEventUser)) {
            dialogTitle = allTitles[0];
        } else if (LinkTypeEventUser.EVENT_INVITED.equals(linkTypeEventUser)) {
            dialogTitle = allTitles[1];
        } else if (LinkTypeEventUser.EVENT_GOING.equals(linkTypeEventUser)) {
            dialogTitle = allTitles[2];
        } else if (LinkTypeEventUser.EVENT_NOT_GOING.equals(linkTypeEventUser)) {
            dialogTitle = allTitles[3];
        }

        UserGroupDialog alertDialog = UserGroupDialog.newInstance(dialogTitle, users != null ? users : new ArrayList<User>());
        alertDialog.show(getFragmentManager(), "alertDialog");
    }

    private void showProgressDialog() {
        if (!(progressDialog != null && progressDialog.isShowing())) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle(getString(R.string.progress_dialog_title_loading));
            progressDialog.setMessage(getString(R.string.progress_dialog_text_loadingDetail));
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private class LoadEventDetailAsyncTask extends AbstractApiAsyncTask<Void, Void, Event> {
        private final String eventId;

        private LoadEventDetailAsyncTask(String eventId) {
            super(AuthUtils.getAccessTokenFromPrefs(getContext()));
            this.eventId = eventId;
        }

        @Override
        protected void onPreExecute() {
            showProgressDialog();
        }

        @Override
        protected Event doInBackground(Void... voids) {
            try {
                final String url = LinkUtils.getCommonLinkByType(LinkTypeCommon.EVENTS) + "/" + eventId;

                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                HttpEntity<?> request = new HttpEntity<>(header);
                ResponseEntity<Event> result = restTemplate.exchange(url, HttpMethod.GET, request, Event.class);

                if (result.getStatusCode().is2xxSuccessful()) return result.getBody();
            } catch (Exception e) {
                if (e instanceof HttpClientErrorException) {
                    Log.e(LOG_TAG, "Error while doing request -> " + ((HttpClientErrorException) e).getResponseBodyAsString());
                }
                Log.e(LOG_TAG, "Error while doing request -> " + e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Event event) {
            if (event != null) {
                setupRecyclerView(event, new ArrayList<Comment>());
                new LoadEventCommentsAsyncTask(event).execute();
                new LoadUserGroupsAsyncTask(event).execute();
            } else {
                hideProgressDialog();
                Toast.makeText(getActivity().getApplicationContext(), "Error occurred while loading event detail!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class LoadUserGroupsAsyncTask extends AbstractApiAsyncTask<Void, Void, Map<LinkTypeEventUser, List<User>>> {
        private final Event event;
        private final List<LinkTypeEventUser> requiredLinkTypeEventUsers;

        public LoadUserGroupsAsyncTask(Event event) {
            super(AuthUtils.getAccessTokenFromPrefs(getContext()));
            this.event = event;

            this.requiredLinkTypeEventUsers = new ArrayList<>();
            this.requiredLinkTypeEventUsers.add(LinkTypeEventUser.EVENT_AUTHORS);
            this.requiredLinkTypeEventUsers.add(LinkTypeEventUser.EVENT_INVITED);
            this.requiredLinkTypeEventUsers.add(LinkTypeEventUser.EVENT_NOT_GOING);
            this.requiredLinkTypeEventUsers.add(LinkTypeEventUser.EVENT_GOING);

            eventUserGroupsMap = new HashMap<>();
        }

        @Override
        protected Map<LinkTypeEventUser, List<User>> doInBackground(Void... voids) {
            try {
                for (LinkTypeEventUser linkTypeEventUser : this.requiredLinkTypeEventUsers) {
                    final String url = PaginationUtils.createPagedUrlWithNumberOrSize(
                            LinkUtils.getLinkHref(event, linkTypeEventUser), PaginationParam.SIZE, PaginationUtils.MAXIMUM_PAGE_SIZE);
                    if (url == null) {
                        return null;
                    }

                    restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                    HttpEntity<?> request = new HttpEntity<>(header);
                    ResponseEntity<UsersContent> result = restTemplate.exchange(url, HttpMethod.GET, request, UsersContent.class);

                    if (!result.getStatusCode().is2xxSuccessful()) {
                        Log.e(LOG_TAG, "Error while loading users of group '" + linkTypeEventUser + "' from server -> " + result.getStatusCode().toString());
                    } else {
                        eventUserGroupsMap.put(linkTypeEventUser, result.getBody().getContent());
                    }
                }

                return eventUserGroupsMap;
            } catch (Exception e) {
                if (e instanceof HttpClientErrorException) {
                    Log.e(LOG_TAG, "Error while doing request -> " + ((HttpClientErrorException) e).getResponseBodyAsString());
                }
                Log.e(LOG_TAG, "Error while doing request -> " + e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Map<LinkTypeEventUser, List<User>> userGroups) {
            if (userGroups != null) {
                if (userGroups.size() != this.requiredLinkTypeEventUsers.size()) {
                    Log.e(LOG_TAG, "Not all required user groups were loaded!");
                }
                showAppropriateContentForUser(this.event);
            } else {
                Toast.makeText(getActivity().getApplicationContext(), "Error occurred while loading event detail!", Toast.LENGTH_SHORT).show();
            }

            hideProgressDialog();
        }
    }

    private class LoadEventCommentsAsyncTask extends AbstractApiAsyncTask<Void, Void, EventCommentsContent> {
        private final Event event;

        private LoadEventCommentsAsyncTask(Event event) {
            super(AuthUtils.getAccessTokenFromPrefs(getContext()));
            this.event = event;
        }

        @Override
        protected EventCommentsContent doInBackground(Void... voids) {
            try {
                final String url = PaginationUtils.createPagedUrlWithSortAndSize(LinkUtils.getLinkHref(event, LinkTypeEventUser.EVENT_COMMENTS),
                        PaginationUtils.MAXIMUM_PAGE_SIZE, PaginationSortType.SORT_TYPE_DESC, "timestamp");
                if (url == null) {
                    return null;
                }

                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                HttpEntity<?> request = new HttpEntity<>(header);
                ResponseEntity<EventCommentsContent> result = restTemplate.exchange(url, HttpMethod.GET, request, EventCommentsContent.class);

                if (result.getStatusCode().is2xxSuccessful()) return result.getBody();
            } catch (Exception e) {
                if (e instanceof HttpClientErrorException) {
                    Log.e(LOG_TAG, "Error while doing request -> " + ((HttpClientErrorException) e).getResponseBodyAsString());
                }
                Log.e(LOG_TAG, "Error while doing request -> " + e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(EventCommentsContent commentsContent) {
            if (commentsContent != null) {
                setupRecyclerView(event, commentsContent.getContent());
            } else {
                Toast.makeText(getActivity().getApplicationContext(), "Error occurred while loading event comments!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class AcceptDeclineInvitationAsyncTask extends AbstractApiAsyncTask<Void, Void, Boolean> {
        private final Event event;
        private final boolean accept;

        private AcceptDeclineInvitationAsyncTask(Event event, boolean accept) {
            super(AuthUtils.getAccessTokenFromPrefs(getContext()));
            this.event = event;
            this.accept = accept;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                final String url = accept ? LinkUtils.getLinkHref(event, LinkTypeEventUser.EVENT_GOING) : LinkUtils.getLinkHref(event, LinkTypeEventUser.EVENT_NOT_GOING);
                if (url == null) {
                    return false;
                }

                HttpEntity<?> request = new HttpEntity<>(header);
                ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
                if (result.getStatusCode().is2xxSuccessful()) return true;
            } catch (Exception e) {
                if (e instanceof HttpClientErrorException) {
                    Log.e(LOG_TAG, "Error while doing request -> " + ((HttpClientErrorException) e).getResponseBodyAsString());
                }
                Log.e(LOG_TAG, "Error while doing request -> " + e.toString());
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                Toast.makeText(getActivity().getApplicationContext(), "You were successfully marked as " + (this.accept ? "going." : "not going."), Toast.LENGTH_SHORT).show();
                setContentVisibilityForUser(!this.accept, this.accept, false);
                moveUserToGroupByAcceptance(this.accept);
            } else {
                Toast.makeText(getActivity().getApplicationContext(), "Error occurred while accepting or declining invitation!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class PostNewCommentAsyncTask extends AbstractApiAsyncTask<Void, Void, Comment> {
        private final Event event;
        private final CommentCreate newComment;

        private PostNewCommentAsyncTask(Event event, CommentCreate newComment) {
            super(AuthUtils.getAccessTokenFromPrefs(getContext()));
            this.event = event;
            this.newComment = newComment;
        }

        @Override
        protected Comment doInBackground(Void... voids) {
            try {
                final String url = LinkUtils.getLinkHref(event, LinkTypeEventUser.EVENT_COMMENTS);
                if (url == null) {
                    return null;
                }

                HttpEntity<CommentCreate> request = new HttpEntity<>(newComment, header);
                ResponseEntity<Comment> result = restTemplate.exchange(url, HttpMethod.POST, request, Comment.class);
                if (result.getStatusCode().is2xxSuccessful()) return result.getBody();
            } catch (Exception e) {
                if (e instanceof HttpClientErrorException) {
                    Log.e(LOG_TAG, "Error while doing request -> " + ((HttpClientErrorException) e).getResponseBodyAsString());
                }
                Log.e(LOG_TAG, "Error while doing request -> " + e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Comment result) {
            if (result != null) {
                new LoadEventCommentsAsyncTask(event).execute();
                ((EditText) eventDetailRecyclerView.getLayoutManager().findViewByPosition(0).findViewById(R.id.event_detail_view_new_comment_text)).setText("");
            } else {
                Toast.makeText(getActivity().getApplicationContext(), "Error while posting new comment to event!", Toast.LENGTH_SHORT).show();
            }

            eventDetailRecyclerView.getLayoutManager().findViewByPosition(0).findViewById(R.id.event_detail_view_add_comment_button).setEnabled(true);
        }
    }

    private class EventDetailViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private static final int VIEW_TYPE_EVENT = 0;
        private static final int VIEW_TYPE_COMMENTS = 1;
        private final List<Comment> comments;
        private final Event event;

        public EventDetailViewAdapter(Event event, List<Comment> comments) {
            this.comments = comments;
            this.event = event;
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0) {
                return VIEW_TYPE_EVENT;
            }

            return VIEW_TYPE_COMMENTS;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == VIEW_TYPE_EVENT) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_detail_event_item, parent, false);
                return new EventDetailViewHolder(view);
            }

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_detail_comment_item, parent, false);
            return new CommentViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
            switch (holder.getItemViewType()) {
                case VIEW_TYPE_EVENT: {
                    if (event != null) {
                        configureEventDetailView((EventDetailViewHolder) holder);
                    }
                    break;
                }
                case VIEW_TYPE_COMMENTS: {
                    configureCommentView((CommentViewHolder) holder, position);
                    break;
                }
            }
        }

        @Override
        public int getItemCount() {
            return comments.size() + 1;
        }

        private void configureCommentView(CommentViewHolder commentViewHolder, int position) {
            commentViewHolder.commentItem = comments.get(position - 1);

            String authorString = comments.get(position - 1).getAuthor().getName() + " " + comments.get(position - 1).getAuthor().getSurname() +
                    " (" + Constants.DATE_AND_TIME_FORMAT.format(comments.get(position - 1).getTimestamp()) + ")";

            commentViewHolder.commentAuthorTextView.setText(authorString);
            commentViewHolder.commentContentTextView.setText(comments.get(position - 1).getContent());
        }

        private void configureEventDetailView(final EventDetailViewHolder eventDetailViewHolder) {
            // fill details
            eventDetailViewHolder.event = event;
            eventDetailViewHolder.titleTextView.setText(event.getTitle());
            eventDetailViewHolder.titleTextView.setVisibility(isTwoPane ? View.VISIBLE : View.GONE);
            eventDetailViewHolder.descriptionTextView.setText(event.getDescription());
            eventDetailViewHolder.dateTextView.setText(Constants.DATE_AND_TIME_FORMAT.format(event.getTimestamp()));
            eventDetailViewHolder.placeTextView.setText(event.getLocation());
            eventDetailViewHolder.typeTextView.setText(EventUtils.getEventTypeTextId(event));
            eventDetailViewHolder.typeImageView.setImageResource(EventUtils.getEventTypeIcon(event));
            eventDetailViewHolder.visibilityTextView.setText(EventUtils.getEventVisibilityTextId(event));
            eventDetailViewHolder.visibilityImageView.setImageResource(EventUtils.getEventVisibilityIcon(event));

            // set toolbar title
            setCollapsingToolbarTitle(event.getTitle());

            // set up users groups spinner
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.event_detail_users_groups, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            eventDetailViewHolder.userGroupsSpinner.setAdapter(adapter);
            eventDetailViewHolder.userGroupsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (eventDetailViewHolder.userGroupsSpinner.getSelectedItemPosition()) {
                        case 0:
                            showUserDialogFragment(LinkTypeEventUser.EVENT_AUTHORS);
                            break;
                        case 1:
                            showUserDialogFragment(LinkTypeEventUser.EVENT_INVITED);
                            break;
                        case 2:
                            showUserDialogFragment(LinkTypeEventUser.EVENT_GOING);
                            break;
                        case 3:
                            showUserDialogFragment(LinkTypeEventUser.EVENT_NOT_GOING);
                            break;
                    }
                }
            });

            // buttons for accept/decline of invitations listeners
            eventDetailViewHolder.acceptInvitationButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AcceptDeclineInvitationAsyncTask(event, true).execute();
                }
            });


            eventDetailViewHolder.declineInvitationButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AcceptDeclineInvitationAsyncTask(event, false).execute();
                }
            });

            // button for event edit
            eventDetailViewHolder.editEventButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showEditActivity(event);
                }
            });

            // set up add comment button
            eventDetailViewHolder.addCommentButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String commentContent = eventDetailViewHolder.addCommentEditText.getText().toString().trim();
                    if (commentContent.isEmpty()) {
                        Toast.makeText(getActivity(), "Comment body can not be empty!", Toast.LENGTH_SHORT).show();
                    } else {
                        new PostNewCommentAsyncTask(event, new CommentCreate(commentContent)).execute();
                        eventDetailViewHolder.addCommentButton.setEnabled(false);
                    }
                }
            });
        }

        private class CommentViewHolder extends RecyclerView.ViewHolder {
            public final View view;
            public final TextView commentAuthorTextView;
            public final TextView commentContentTextView;
            public Comment commentItem;

            public CommentViewHolder(View view) {
                super(view);
                this.view = view;
                this.commentAuthorTextView = (TextView) view.findViewById(R.id.event_detail_comment_author);
                this.commentContentTextView = (TextView) view.findViewById(R.id.event_detail_comment_content);
            }
        }

        private class EventDetailViewHolder extends RecyclerView.ViewHolder {
            public final View view;
            public final TextView titleTextView;
            public final TextView descriptionTextView;
            public final TextView dateTextView;
            public final TextView placeTextView;
            public final TextView typeTextView;
            public final ImageView typeImageView;
            public final TextView visibilityTextView;
            public final ImageView visibilityImageView;

            public final TextView userGroupTextView;
            public final Spinner userGroupsSpinner;
            public final Button userGroupsButton;
            public final Button addCommentButton;
            public final EditText addCommentEditText;

            public final Button acceptInvitationButton;
            public final Button declineInvitationButton;
            public final Button editEventButton;

            public Event event;

            public EventDetailViewHolder(View view) {
                super(view);
                this.view = view;
                this.titleTextView = (TextView) view.findViewById(R.id.event_detail_view_title);
                this.descriptionTextView = (TextView) view.findViewById(R.id.event_detail_view_description);
                this.dateTextView = (TextView) view.findViewById(R.id.event_detail_view_date_text);
                this.placeTextView = (TextView) view.findViewById(R.id.event_detail_view_place_text);
                this.typeTextView = (TextView) view.findViewById(R.id.event_detail_view_type_text);
                this.typeImageView = (ImageView) view.findViewById(R.id.event_detail_view_type_icon);
                this.visibilityTextView = (TextView) view.findViewById(R.id.event_detail_view_visibility_text);
                this.visibilityImageView = (ImageView) view.findViewById(R.id.event_detail_view_visibility_icon);

                this.userGroupTextView = (TextView) view.findViewById(R.id.event_detail_view_user_group_text);
                this.userGroupsSpinner = (Spinner) view.findViewById(R.id.event_detail_view_user_groups_spinner);
                this.userGroupsButton = (Button) view.findViewById(R.id.event_detail_view_user_groups_button);
                this.addCommentButton = (Button) view.findViewById(R.id.event_detail_view_add_comment_button);
                this.addCommentEditText = (EditText) view.findViewById(R.id.event_detail_view_new_comment_text);

                this.acceptInvitationButton = (Button) view.findViewById(R.id.event_detail_view_invitation_accept);
                this.declineInvitationButton = (Button) view.findViewById(R.id.event_detail_view_invitation_decline);
                this.editEventButton = (Button) view.findViewById(R.id.event_detail_view_edit_button);
            }
        }
    }
}
