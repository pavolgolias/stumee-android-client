package sk.stuba.fei.stumee.utils.pagination;

public enum PaginationParam {
    PAGE,
    SIZE,
    SORT
}
