package sk.stuba.fei.stumee.utils.pagination;

public enum PaginationSortType {
    SORT_TYPE_ACS,
    SORT_TYPE_DESC
}
