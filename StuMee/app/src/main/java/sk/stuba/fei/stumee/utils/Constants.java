package sk.stuba.fei.stumee.utils;

import java.text.SimpleDateFormat;

public class Constants {
    // token
    public static final String TOKEN_GROUP_PREFS_KEY = "sk.stumee.token";
    public static final String ACCESS_TOKEN_PREFS_KEY = "sk.stumee.token.access";
    public static final String REFRESH_TOKEN_PREFS_KEY = "sk.stumee.token.refresh";
    public static final String EXPIRATION_TOKEN_PREFS_KEY = "sk.stumee.token.expiration";
    public static final int MIN_TOKEN_LIFE_TIME_MILLIS = 1500000; // 25 min - if token expiration is closer than this time we refresh the token

    // user detail
    public static final String USER_GROUP_PREFS_KEY = "sk.stumee.user";
    public static final String USER_JSON_PREFS_KEY = "sk.stumee.user.json";
    public static final String USER_PICTURE_NAME = "StumeeUserPicture";

    // date formats
    public static final SimpleDateFormat DATE_FORMAT;
    public static final SimpleDateFormat TIME_FORMAT;
    public static final SimpleDateFormat DATE_AND_TIME_FORMAT;

    static {
        DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");
        TIME_FORMAT = new SimpleDateFormat("HH:mm");
        DATE_AND_TIME_FORMAT = new SimpleDateFormat("dd.MM.yyyy, HH:mm");
        // DATE_FORMAT = SimpleDateFormat.getDateInstance();
        // TIME_FORMAT = SimpleDateFormat.getTimeInstance();
        // DATE_AND_TIME_FORMAT = SimpleDateFormat.getDateTimeInstance();
    }
}