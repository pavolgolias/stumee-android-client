package sk.stuba.fei.stumee.utils.linking;

public enum LinkTypeCommon {
    // "147.175.180.84:8080" || 10.0.3.2:8080"
    SERVER_BASE_URL("http://stumee-andrejkosar.rhcloud.com/"),
    SERVER_BASE_API_URL("http://stumee-andrejkosar.rhcloud.com/api"),

    OAUTH_TOKEN_ENDPOINT("/oauth/token"),
    OAUTH_REVOKE_TOKEN_ENDPOINT("/oauth/revoke-token"),
    OAUTH_AIS_AUTOCOMPLETE("/ais/users/search"),

    USERS("/users"),
    USERS_ME("/users/me"),
    USERS_SEARCH("/users/search"),

    EVENTS("/events"),
    EVENTS_CONFIRMED("/events/confirmed"),
    EVENTS_UNCONFIRMED("/events/unconfirmed"),
    EVENTS_OWN("/events/own"),
    EVENTS_DECLINED("/events/declined"),
    EVENTS_OLD("/events/old"),
    EVENTS_SEARCH("/events/search");

    private final String linkUrl;

    LinkTypeCommon(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getLinkUrl() {
        return linkUrl;
    }
}
