package sk.stuba.fei.stumee.model.event;

public enum EventType {
    STUDY,
    FUN,
    SPORT,
    FOOD,
    FREE_TIME,
    WORK,
    OTHER
}
