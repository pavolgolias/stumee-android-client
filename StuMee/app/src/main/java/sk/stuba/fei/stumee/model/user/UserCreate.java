package sk.stuba.fei.stumee.model.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserCreate {
    private int id;
    private String username;
    private String stuEmail;
    private String name;
    private String surname;
    private String stuPassword;

    public UserCreate() {
    }

    public UserCreate(int id, String username, String stuEmail, String name, String surname, String stuPassword) {
        this.id = id;
        this.username = username;
        this.stuEmail = stuEmail;
        this.name = name;
        this.surname = surname;
        this.stuPassword = stuPassword;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getStuEmail() {
        return stuEmail;
    }

    public void setStuEmail(String stuEmail) {
        this.stuEmail = stuEmail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getStuPassword() {
        return stuPassword;
    }

    public void setStuPassword(String stuPassword) {
        this.stuPassword = stuPassword;
    }

    @Override
    public String toString() {
        return "UserCreate{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", stuEmail='" + stuEmail + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", stuPassword='" + stuPassword + '\'' +
                '}';
    }
}
