package sk.stuba.fei.stumee.model.user.regAutocomplete;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

import sk.stuba.fei.stumee.model.Link;

@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegAutocompleteResponse {
    private List<Link> links;
    private List<RegAutocompleteResponseContent> content;

    public RegAutocompleteResponse() {
    }

    public RegAutocompleteResponse(List<Link> links, List<RegAutocompleteResponseContent> content) {
        this.links = links;
        this.content = content;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    public List<RegAutocompleteResponseContent> getContent() {
        return content;
    }

    public void setContent(List<RegAutocompleteResponseContent> content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "RegAutocompleteResponse{" +
                "links=" + links +
                ", content=" + content +
                '}';
    }
}
