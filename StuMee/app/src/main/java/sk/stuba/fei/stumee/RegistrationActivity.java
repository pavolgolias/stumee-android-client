package sk.stuba.fei.stumee;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import sk.stuba.fei.stumee.dialogs.RegistrationOptionsDialogFragment;
import sk.stuba.fei.stumee.model.user.User;
import sk.stuba.fei.stumee.model.user.UserCreate;
import sk.stuba.fei.stumee.model.user.regAutocomplete.RegAutocompleteRequest;
import sk.stuba.fei.stumee.model.user.regAutocomplete.RegAutocompleteResponse;
import sk.stuba.fei.stumee.model.user.regAutocomplete.RegAutocompleteResponseContent;
import sk.stuba.fei.stumee.utils.UserUtils;
import sk.stuba.fei.stumee.utils.linking.LinkTypeCommon;
import sk.stuba.fei.stumee.utils.linking.LinkUtils;


public class RegistrationActivity extends AppCompatActivity implements RegistrationOptionsDialogFragment.RegistrationOptionsDialogListener {
    private static final String LOG_TAG = "RegistrationActivity";

    private EditText nickNameEditText;
    private EditText emailEditText;
    private EditText passwordEditText;
    private ProgressBar progressBar;
    private ScrollView registrationFormView;
    private boolean isProgressShown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        nickNameEditText = (EditText) findViewById(R.id.registration_nick_name);
        emailEditText = (EditText) findViewById(R.id.registration_email);
        passwordEditText = (EditText) findViewById(R.id.registration_password);
        progressBar = (ProgressBar) findViewById(R.id.registration_progress_bar);
        registrationFormView = (ScrollView) findViewById(R.id.registration_form);
        Button registrationSubmitButton = (Button) findViewById(R.id.registration_submit_button);

        if (registrationSubmitButton != null) {
            registrationSubmitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    attemptRegistration();
                }
            });
        }

        showProgress(false);
    }

    private void showProgress(final boolean show) {
        this.isProgressShown = show;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            registrationFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            registrationFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    registrationFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            progressBar.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            registrationFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private void attemptRegistration() {
        // Reset errors
        nickNameEditText.setError(null);
        emailEditText.setError(null);
        passwordEditText.setError(null);

        boolean isError = false;
        View focusView = null;

        String userName = nickNameEditText.getText().toString();
        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        if (!UserUtils.isNickNameValid(userName)) {
            nickNameEditText.setError("Nick name is not correct!");
            focusView = nickNameEditText;
            isError = true;
        }

        if (!UserUtils.isStuEmailValid(email)) {
            emailEditText.setError("Email is not correct!");
            focusView = emailEditText;
            isError = true;
        }

        if (!UserUtils.isPasswordValid(password)) {
            passwordEditText.setError("Password is not correct!");
            focusView = passwordEditText;
            isError = true;
        }

        if (isError) {
            focusView.requestFocus();
        } else {
            new ValidateUserAisEmailAsyncTask(userName, email, password).execute();
        }
    }

    private void clearInputForm() {
        nickNameEditText.setText("");
        emailEditText.setText("");
        passwordEditText.setText("");
    }

    private void showRegPossibilitiesDialog(List<RegAutocompleteResponseContent> contentList, String userName, String email, String password) {
        RegistrationOptionsDialogFragment dialog = RegistrationOptionsDialogFragment.newInstance(contentList, userName, email, password);
        dialog.show(getSupportFragmentManager(), "registrationOptionsDialog");
    }

    @Override
    public void onRegOptionsDialogPositiveClick(UserCreate userCreate) {
        new RegisterUserAsyncTask(userCreate).execute();
    }

    private class ValidateUserAisEmailAsyncTask extends AsyncTask<Void, Void, RegAutocompleteResponse> {
        private final String userName;
        private final String email;
        private final String password;

        private ValidateUserAisEmailAsyncTask(String userName, String email, String password) {
            this.userName = userName;
            this.email = email;
            this.password = password;
        }

        @Override
        protected void onPreExecute() {
            showProgress(true);
        }

        @Override
        protected RegAutocompleteResponse doInBackground(Void... voids) {
            try {
                final String url = LinkUtils.getCommonLinkByType(LinkTypeCommon.OAUTH_AIS_AUTOCOMPLETE);
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                ResponseEntity<RegAutocompleteResponse> result = restTemplate.postForEntity(url, new RegAutocompleteRequest(this.email, 10), RegAutocompleteResponse.class);

                if (result.getStatusCode().is2xxSuccessful()) return result.getBody();
            } catch (Exception e) {
                if (e instanceof HttpClientErrorException) {
                    Log.e(LOG_TAG, "Error while doing request -> " + ((HttpClientErrorException) e).getResponseBodyAsString());
                }
                Log.e(LOG_TAG, "Error while doing request -> " + e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(RegAutocompleteResponse response) {
            if (response != null && response.getContent().size() > 0) {
                if (response.getContent().size() == 1) {
                    RegAutocompleteResponseContent responseContent = response.getContent().get(0);
                    String name = UserUtils.getNameFromWholeAisName(responseContent.getWholeName());
                    String surname = UserUtils.getSurnameFromWholeAisName(responseContent.getWholeName());

                    UserCreate userCreate = new UserCreate(responseContent.getId(), this.userName, this.email, name, surname, this.password);
                    new RegisterUserAsyncTask(userCreate).execute();
                } else {
                    showProgress(false);
                    showRegPossibilitiesDialog(response.getContent(), this.userName, this.email, this.password);
                }
            } else {
                showProgress(false);
                Toast.makeText(getApplicationContext(), "It was unable to verify your AIS email! Please, try again!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class RegisterUserAsyncTask extends AsyncTask<Void, Void, User> {
        private final UserCreate userCreate;

        private RegisterUserAsyncTask(UserCreate userCreate) {
            this.userCreate = userCreate;
        }

        @Override
        protected void onPreExecute() {
            if (!isProgressShown) {
                showProgress(true);
            }
        }

        @Override
        protected User doInBackground(Void... voids) {
            try {
                final String url = LinkUtils.getCommonLinkByType(LinkTypeCommon.USERS);
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                ResponseEntity<User> result = restTemplate.postForEntity(url, userCreate, User.class);

                if (result.getStatusCode().is2xxSuccessful()) return result.getBody();
            } catch (Exception e) {
                if (e instanceof HttpClientErrorException) {
                    Log.e(LOG_TAG, "Error while doing request -> " + ((HttpClientErrorException) e).getResponseBodyAsString());
                }
                Log.e(LOG_TAG, "Error while doing request -> " + e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(User user) {
            if (user != null) {
                Toast.makeText(getApplicationContext(), "Registration successful!", Toast.LENGTH_SHORT).show();
                clearInputForm();
            } else {
                Toast.makeText(getApplicationContext(), "It was unable to complete registration! Please, check your AIS email and password and try again!", Toast.LENGTH_LONG).show();
            }

            showProgress(false);
        }
    }
}
