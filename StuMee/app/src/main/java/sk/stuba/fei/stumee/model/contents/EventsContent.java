package sk.stuba.fei.stumee.model.contents;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

import sk.stuba.fei.stumee.model.Page;
import sk.stuba.fei.stumee.model.event.Event;
import sk.stuba.fei.stumee.model.Link;

@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
public class EventsContent {
    private List<Link> links;
    private List<Event> content;
    private Page page;

    public EventsContent() {
    }

    public EventsContent(List<Link> links, List<Event> content, Page page) {
        this.links = links;
        this.content = content;
        this.page = page;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    public List<Event> getContent() {
        return content;
    }

    public void setContent(List<Event> content) {
        this.content = content;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    @Override
    public String toString() {
        return "EventsContent{" +
                "links=" + links +
                ", content=" + content +
                ", page=" + page +
                '}';
    }
}
