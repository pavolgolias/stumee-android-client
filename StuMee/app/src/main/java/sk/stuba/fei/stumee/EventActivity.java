package sk.stuba.fei.stumee;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import sk.stuba.fei.stumee.asyncTasks.AbstractApiAsyncTask;
import sk.stuba.fei.stumee.asyncTasks.AbstractAuthAsyncTask;
import sk.stuba.fei.stumee.dialogs.AboutDialogFragment;
import sk.stuba.fei.stumee.dividers.DividerItemDecoration;
import sk.stuba.fei.stumee.model.SearchRequestObject;
import sk.stuba.fei.stumee.model.contents.EventsContent;
import sk.stuba.fei.stumee.model.event.Event;
import sk.stuba.fei.stumee.model.user.User;
import sk.stuba.fei.stumee.model.user.UserCredentials;
import sk.stuba.fei.stumee.model.user.auth.RevokeAccessTokenObject;
import sk.stuba.fei.stumee.utils.AuthUtils;
import sk.stuba.fei.stumee.utils.ConnectionUtils;
import sk.stuba.fei.stumee.utils.Constants;
import sk.stuba.fei.stumee.utils.EventUtils;
import sk.stuba.fei.stumee.utils.UserUtils;
import sk.stuba.fei.stumee.utils.linking.LinkTypeCommon;
import sk.stuba.fei.stumee.utils.linking.LinkTypeEventUser;
import sk.stuba.fei.stumee.utils.linking.LinkUtils;
import sk.stuba.fei.stumee.utils.pagination.PaginationParam;
import sk.stuba.fei.stumee.utils.pagination.PaginationUtils;

public class EventActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final String LOG_TAG = "EventActivity";

    private User currentUser;
    private ProgressDialog progressDialog;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView navDrawerHeaderNameView;
    private TextView navDrawerHeaderEmailView;
    private ImageView navDrawerHeaderImageView;
    private boolean isTwoPane;
    private RecyclerView recyclerView;
    private NavigationView navigationView;
    private LinkTypeCommon currentFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar_toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_events_activity);
        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ConnectionUtils.isDeviceConnected(getApplicationContext(), true)) {
                        Intent intent = new Intent(getApplicationContext(), CreateEditEventActivity.class);
                        startActivity(intent);
                    }
                }
            });
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        if (drawer != null) {
            drawer.addDrawerListener(toggle);
        }
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(this);
            navDrawerHeaderNameView = (TextView) navigationView.getHeaderView(0).findViewById(R.id.nav_header_user_name);
            navDrawerHeaderEmailView = (TextView) navigationView.getHeaderView(0).findViewById(R.id.nav_header_user_email);
            navDrawerHeaderImageView = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.nav_header_user_picture);
        }

        if (findViewById(R.id.event_detail_container) != null) {
            isTwoPane = true;
        }

        recyclerView = (RecyclerView) findViewById(R.id.events_list);
        setupRecyclerView(new ArrayList<Event>());

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.events_list_swipe_refresh);
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    loadEvents();
                }
            });
        }

        if (ConnectionUtils.isDeviceConnected(getApplicationContext(), true)) {
            validateAccessToken();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.events_activity_menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!ConnectionUtils.isDeviceConnected(getApplicationContext(), true)) {
                    return false;
                }

                if (query != null && query.length() > 2) {
                    new SearchEventsAsyncTask(query).execute();
                    return true;
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // do nothing
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_about) {
            AboutDialogFragment aboutDialogFragment = new AboutDialogFragment();
            aboutDialogFragment.show(getSupportFragmentManager(), "AboutDialogFragment");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.drawer_filer_home) {
            currentFilter = LinkTypeCommon.EVENTS_CONFIRMED;
            loadEvents();
        } else if (id == R.id.drawer_filter_unconfirmed) {
            currentFilter = LinkTypeCommon.EVENTS_UNCONFIRMED;
            loadEvents();
        } else if (id == R.id.drawer_filter_own) {
            currentFilter = LinkTypeCommon.EVENTS_OWN;
            loadEvents();
        } else if (id == R.id.drawer_filter_declined) {
            currentFilter = LinkTypeCommon.EVENTS_DECLINED;
            loadEvents();
        } else if (id == R.id.drawer_filter_old) {
            currentFilter = LinkTypeCommon.EVENTS_OLD;
            loadEvents();
        } else if (id == R.id.drawer_user_profile) {
            if (ConnectionUtils.isDeviceConnected(getApplicationContext(), true)) {
                Intent intent = new Intent(this, UserProfileActivity.class);
                startActivity(intent);
            }
        } else if (id == R.id.drawer_user_logout) {
            new LogoutUserAsyncTask().execute();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) {
            drawer.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    private void setupRecyclerView(@NonNull List<Event> events) {
        if (recyclerView != null) {
            recyclerView.setAdapter(new EventsRecyclerViewAdapter(events));
            recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext()));
        }
    }

    private void validateAccessToken() {
        SharedPreferences preferences = getSharedPreferences(Constants.TOKEN_GROUP_PREFS_KEY, 0);
        String accessToken = preferences.getString(Constants.ACCESS_TOKEN_PREFS_KEY, "");
        String refreshToken = preferences.getString(Constants.REFRESH_TOKEN_PREFS_KEY, "");
        Long expirationTime = preferences.getLong(Constants.EXPIRATION_TOKEN_PREFS_KEY, 0);

        if (accessToken.isEmpty() || refreshToken.isEmpty() || expirationTime == 0) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        } else if ((Calendar.getInstance().getTimeInMillis() + Constants.MIN_TOKEN_LIFE_TIME_MILLIS) > expirationTime) {
            new RefreshAccessTokenAsyncTask(refreshToken).execute();
        } else {
            loadEvents();
            loadUserDetail();
        }
    }

    private void loadEvents() {
        if (ConnectionUtils.isDeviceConnected(getApplicationContext(), true)) {
            if (currentFilter == null) {
                navigationView.setCheckedItem(R.id.drawer_filer_home);
                new LoadEventsAsyncTask(LinkTypeCommon.EVENTS_CONFIRMED).execute();
            } else {
                new LoadEventsAsyncTask(currentFilter).execute();
            }
        }
    }

    private void loadUserDetail() {
        currentUser = UserUtils.loadUserObjectFromPrefs(getApplicationContext());
        if (currentUser == null) {
            new LoadUserDetailAsyncTask().execute();
        } else {
            updateNavigationDrawerUserDetail();
            updateNavigationDrawerUserPhoto(UserUtils.loadUserPictureFromFile(getApplicationContext()));
        }
    }

    private void updateNavigationDrawerUserDetail() {
        if (currentUser == null) {
            return;
        }

        String name = currentUser.getName() + " " + currentUser.getSurname();
        navDrawerHeaderNameView.setText(name);
        navDrawerHeaderEmailView.setText(currentUser.getStuEmail());
    }

    private void updateNavigationDrawerUserPhoto(Bitmap bitmap) {
        if (bitmap != null) {
            navDrawerHeaderImageView.setImageBitmap(bitmap);
        } else {
            navDrawerHeaderImageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.user_avatar));
        }
    }

    private void showProgressDialog(int messageId) {
        if (!isProgressDialogShowing()) {
            progressDialog = new ProgressDialog(EventActivity.this);
            progressDialog.setTitle(getString(R.string.progress_dialog_title_loading));
            progressDialog.setMessage(getString(messageId));
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (isProgressDialogShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private boolean isProgressDialogShowing() {
        return progressDialog != null && progressDialog.isShowing();
    }

    private class LoadEventsAsyncTask extends AbstractApiAsyncTask<Void, Void, EventsContent> {
        private final LinkTypeCommon linkType;
        private boolean invalidAccessToken = false;

        LoadEventsAsyncTask(LinkTypeCommon linkType) {
            super(AuthUtils.getAccessTokenFromPrefs(getApplicationContext()));
            this.linkType = linkType;
        }

        @Override
        protected void onPreExecute() {
            if (!(swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())) {
                showProgressDialog(R.string.progress_dialog_text_loadEvents);
            }
        }

        @Override
        protected EventsContent doInBackground(Void... voids) {
            try {
                String url = LinkUtils.getCommonLinkByType(linkType);
                url = PaginationUtils.createPagedUrlWithNumberOrSize(url, PaginationParam.SIZE, PaginationUtils.MAXIMUM_PAGE_SIZE);
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                HttpEntity<?> request = new HttpEntity<>(header);
                ResponseEntity<EventsContent> result = restTemplate.exchange(url, HttpMethod.GET, request, EventsContent.class);

                if (result.getStatusCode().is2xxSuccessful()) return result.getBody();
            } catch (Exception e) {
                if (e instanceof HttpClientErrorException) {
                    Log.e(LOG_TAG, "Error while doing request -> " + ((HttpClientErrorException) e).getResponseBodyAsString());
                    if (((HttpClientErrorException) e).getStatusCode().equals(HttpStatus.UNAUTHORIZED)) {
                        invalidAccessToken = true;
                    }
                }
                Log.e(LOG_TAG, "Error while doing request -> " + e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(EventsContent events) {
            hideProgressDialog();
            if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }

            if (events != null) {
                setupRecyclerView(events.getContent());
            } else if (invalidAccessToken) {
                SharedPreferences preferences = getSharedPreferences(Constants.TOKEN_GROUP_PREFS_KEY, 0);
                new RefreshAccessTokenAsyncTask(preferences.getString(Constants.REFRESH_TOKEN_PREFS_KEY, "")).execute();
            } else {
                Toast.makeText(getApplicationContext(), "Error occurred while loading events!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class LoadUserDetailAsyncTask extends AbstractApiAsyncTask<Void, Void, User> {

        LoadUserDetailAsyncTask() {
            super(AuthUtils.getAccessTokenFromPrefs(getApplicationContext()));
        }

        @Override
        protected User doInBackground(Void... voids) {
            try {
                final String url = LinkUtils.getCommonLinkByType(LinkTypeCommon.USERS_ME);
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                HttpEntity<?> request = new HttpEntity<>(header);
                ResponseEntity<User> result = restTemplate.exchange(url, HttpMethod.GET, request, User.class);

                if (result.getStatusCode().is2xxSuccessful()) return result.getBody();
            } catch (Exception e) {
                if (e instanceof HttpClientErrorException) {
                    Log.e(LOG_TAG, "Error while doing request -> " + ((HttpClientErrorException) e).getResponseBodyAsString());
                }
                Log.e(LOG_TAG, "Error while doing request -> " + e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(User user) {
            if (user != null) {
                currentUser = user;
                UserUtils.saveUserObjectToPrefs(getApplicationContext(), user);
                updateNavigationDrawerUserDetail();
                new DownloadUserPictureAsyncTask(user).execute();
            } else {
                Toast.makeText(getApplicationContext(), "Error occurred while loading user profile details!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class DownloadUserPictureAsyncTask extends AbstractApiAsyncTask<Void, Void, byte[]> {
        private final User user;

        DownloadUserPictureAsyncTask(User user) {
            super(AuthUtils.getAccessTokenFromPrefs(getApplicationContext()));
            this.user = user;
        }

        @Override
        protected byte[] doInBackground(Void... voids) {
            try {
                final String url = LinkUtils.getLinkHref(user, LinkTypeEventUser.USER_PICTURE_DATA);
                restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());

                header.add("Content-Type", "application/octet-stream");
                HttpEntity<?> request = new HttpEntity<>(header);
                ResponseEntity<byte[]> result = restTemplate.exchange(url, HttpMethod.GET, request, byte[].class);

                if (result.getStatusCode().is2xxSuccessful()) return result.getBody();
            } catch (Exception e) {
                if (e instanceof HttpClientErrorException) {
                    Log.e(LOG_TAG, "Error while doing request -> " + ((HttpClientErrorException) e).getResponseBodyAsString());
                }
                Log.e(LOG_TAG, "Error while doing request -> " + e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(byte[] input) {
            if (input != null) {
                UserUtils.saveUserPictureToFile(getApplicationContext(), input);
                updateNavigationDrawerUserPhoto(UserUtils.loadUserPictureFromFile(getApplicationContext()));
            } else {
                Log.e(LOG_TAG, "Error while downloading user profile photo!");
                updateNavigationDrawerUserPhoto(null);
            }
        }
    }

    private class RefreshAccessTokenAsyncTask extends AbstractAuthAsyncTask<Void, Void, UserCredentials> {
        private RefreshAccessTokenAsyncTask(String refreshToken) {
            super(refreshToken);
        }

        @Override
        protected void onPreExecute() {
            showProgressDialog(R.string.progress_dialog_text_loggingIn);
        }

        @Override
        protected UserCredentials doInBackground(Void... voids) {
            try {
                ResponseEntity<UserCredentials> result = restTemplate.exchange(url, HttpMethod.POST, requestEntity, UserCredentials.class);
                if (result.getStatusCode().is2xxSuccessful()) return result.getBody();
            } catch (Exception e) {
                if (e instanceof HttpClientErrorException) {
                    Log.e(LOG_TAG, "Error while doing request -> " + ((HttpClientErrorException) e).getResponseBodyAsString());
                }
                Log.e(LOG_TAG, "Error while doing request -> " + e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(UserCredentials userCredentials) {
            hideProgressDialog();
            if (userCredentials != null) {
                AuthUtils.saveAuthCredentialsToPrefs(getApplicationContext(), userCredentials.getAccessToken(),
                        userCredentials.getRefreshToken(), userCredentials.getExpiresIn());
                loadEvents();
                loadUserDetail();
            } else {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }

    private class LogoutUserAsyncTask extends AbstractApiAsyncTask<Void, Void, Boolean> {

        LogoutUserAsyncTask() {
            super(AuthUtils.getAccessTokenFromPrefs(getApplicationContext()));
        }

        @Override
        protected void onPreExecute() {
            showProgressDialog(R.string.progress_dialog_text_logout);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                final String url = LinkUtils.getCommonLinkByType(LinkTypeCommon.OAUTH_REVOKE_TOKEN_ENDPOINT);
                HttpEntity<RevokeAccessTokenObject> request = new HttpEntity<>(new RevokeAccessTokenObject(accessToken), header);
                ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.POST, request, String.class);

                if (result.getStatusCode().is2xxSuccessful()) return true;
            } catch (Exception e) {
                if (e instanceof HttpClientErrorException) {
                    Log.e(LOG_TAG, "Error while doing request -> " + ((HttpClientErrorException) e).getResponseBodyAsString());
                }
                Log.e(LOG_TAG, "Error while doing request -> " + e.toString());
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            hideProgressDialog();
            if (!success) {
                Log.e(LOG_TAG, "Error occurred while performing logout.");
            }

            AuthUtils.clearAuthCredentialsFromPrefs(getApplicationContext());
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private class SearchEventsAsyncTask extends AbstractApiAsyncTask<Void, Void, EventsContent> {
        private final String keyWord;

        SearchEventsAsyncTask(String keyWord) {
            super(AuthUtils.getAccessTokenFromPrefs(getApplicationContext()));
            this.keyWord = keyWord;
        }

        @Override
        protected void onPreExecute() {
            showProgressDialog(R.string.progress_dialog_text_loadEvents);
        }

        @Override
        protected EventsContent doInBackground(Void... voids) {
            try {
                String url = LinkUtils.getCommonLinkByType(LinkTypeCommon.EVENTS_SEARCH);
                url = PaginationUtils.createPagedUrlWithNumberOrSize(url, PaginationParam.SIZE, PaginationUtils.MAXIMUM_PAGE_SIZE);
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                HttpEntity<SearchRequestObject> request = new HttpEntity<>(new SearchRequestObject(keyWord), header);
                ResponseEntity<EventsContent> result = restTemplate.exchange(url, HttpMethod.POST, request, EventsContent.class);

                if (result.getStatusCode().is2xxSuccessful()) return result.getBody();
            } catch (Exception e) {
                if (e instanceof HttpClientErrorException) {
                    Log.e(LOG_TAG, "Error while doing request -> " + ((HttpClientErrorException) e).getResponseBodyAsString());
                }
                Log.e(LOG_TAG, "Error while doing request -> " + e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(EventsContent events) {
            hideProgressDialog();
            if (events != null) {
                setupRecyclerView(events.getContent());
            } else {
                Toast.makeText(getApplicationContext(), "Error occurred while searching events!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class EventsRecyclerViewAdapter extends RecyclerView.Adapter<EventsRecyclerViewAdapter.EventItemViewHolder> {
        private final List<Event> events;

        public EventsRecyclerViewAdapter(List<Event> events) {
            this.events = events;
        }

        @Override
        public EventItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_list_content, parent, false);
            return new EventItemViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final EventItemViewHolder holder, int position) {
            holder.eventItem = events.get(position);
            holder.titleTextView.setText(events.get(position).getTitle());
            holder.placeTextView.setText(events.get(position).getLocation());
            holder.dateTextView.setText(Constants.DATE_FORMAT.format(events.get(position).getTimestamp()));
            holder.timeTextView.setText(Constants.TIME_FORMAT.format(events.get(position).getTimestamp()));
            holder.iconImageView.setImageResource(EventUtils.getEventTypeIcon(events.get(position)));

            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isTwoPane) {
                        Bundle arguments = new Bundle();
                        arguments.putString(EventDetailFragment.ARGS_EVENT_ID_ARGUMENT, String.valueOf(holder.eventItem.getId()));
                        arguments.putBoolean(EventDetailFragment.ARGS_IS_TWO_PANE_MODE, true);
                        arguments.putString(EventDetailFragment.ARGS_CURRENT_USER, UserUtils.getJsonFromUserObject(currentUser));
                        EventDetailFragment fragment = new EventDetailFragment();
                        fragment.setArguments(arguments);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.event_detail_container, fragment)
                                .commit();
                    } else {
                        Context context = v.getContext();
                        Intent intent = new Intent(context, EventDetailActivity.class);
                        intent.putExtra(EventDetailFragment.ARGS_EVENT_ID_ARGUMENT, String.valueOf(holder.eventItem.getId()));
                        intent.putExtra(EventDetailFragment.ARGS_CURRENT_USER, UserUtils.getJsonFromUserObject(currentUser));
                        context.startActivity(intent);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return events.size();
        }

        public class EventItemViewHolder extends RecyclerView.ViewHolder {
            public final View view;
            public final ImageView iconImageView;
            public final TextView titleTextView;
            public final TextView placeTextView;
            public final TextView dateTextView;
            public final TextView timeTextView;
            public Event eventItem;

            public EventItemViewHolder(View view) {
                super(view);
                this.view = view;
                iconImageView = (ImageView) view.findViewById(R.id.list_icon);
                titleTextView = (TextView) view.findViewById(R.id.list_title);
                placeTextView = (TextView) view.findViewById(R.id.list_place);
                dateTextView = (TextView) view.findViewById(R.id.list_date);
                timeTextView = (TextView) view.findViewById(R.id.list_time);
            }
        }
    }
}