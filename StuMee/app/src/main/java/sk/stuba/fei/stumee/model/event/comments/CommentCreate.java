package sk.stuba.fei.stumee.model.event.comments;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CommentCreate {
    private String content;

    public CommentCreate() {
    }

    public CommentCreate(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "CommentCreate{" +
                "content='" + content + '\'' +
                '}';
    }
}
