package sk.stuba.fei.stumee.utils;

import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import sk.stuba.fei.stumee.R;
import sk.stuba.fei.stumee.model.event.Event;
import sk.stuba.fei.stumee.model.event.EventCreate;
import sk.stuba.fei.stumee.model.event.EventType;
import sk.stuba.fei.stumee.model.event.EventVisibility;

public class EventUtils {
    private static final String LOG_TAG = "EventUtils";

    public static int getEventTypeIcon(Event event) {
        return getEventTypeIcon(event.getType());
    }

    public static int getEventTypeIcon(EventType eventType) {
        switch (eventType) {
            case STUDY:
                return R.drawable.ic_school_black_24dp;
            case SPORT:
                return R.drawable.ic_fitness_center_black_24dp;
            case FUN:
                return R.drawable.ic_sentiment_very_satisfied_black_24dp;
            case FOOD:
                return R.drawable.ic_restaurant_black_24dp;
            case FREE_TIME:
                return R.drawable.ic_local_cafe_black_24dp;
            case WORK:
                return R.drawable.ic_work_black_24dp;
        }

        return R.drawable.ic_more_horiz_black_24dp;
    }

    public static int getEventTypeTextId(Event event) {
        return getEventTypeTextId(event.getType());
    }

    public static int getEventTypeTextId(EventType eventType) {
        switch (eventType) {
            case STUDY:
                return R.string.event_type_study;
            case SPORT:
                return R.string.event_type_sport;
            case FUN:
                return R.string.event_type_fun;
            case FOOD:
                return R.string.event_type_food;
            case FREE_TIME:
                return R.string.event_type_freeTime;
            case WORK:
                return R.string.event_type_work;
        }

        return R.string.event_type_other;
    }

    public static int getEventVisibilityTextId(Event event) {
        return getEventVisibilityTextId(event.getVisibility());
    }

    public static int getEventVisibilityTextId(EventVisibility eventVisibility) {
        if (eventVisibility.equals(EventVisibility.PRIVATE)) {
            return R.string.event_visibility_private;
        }

        return R.string.event_visibility_public;
    }

    public static int getEventVisibilityIcon(Event event) {
        return getEventVisibilityIcon(event.getVisibility());
    }

    public static int getEventVisibilityIcon(EventVisibility eventVisibility) {
        if (eventVisibility.equals(EventVisibility.PRIVATE)) {
            return R.drawable.ic_vpn_key_black_24dp;
        }

        return R.drawable.ic_public_black_24dp;
    }

    public static List<EventType> getListOfAllEventTypes() {
        List<EventType> eventTypeList = new ArrayList<>();
        eventTypeList.add(EventType.STUDY);
        eventTypeList.add(EventType.FUN);
        eventTypeList.add(EventType.SPORT);
        eventTypeList.add(EventType.FOOD);
        eventTypeList.add(EventType.FREE_TIME);
        eventTypeList.add(EventType.WORK);
        eventTypeList.add(EventType.OTHER);

        return eventTypeList;
    }

    public static int getTypeIndexFromAllEventTypes(EventType eventType) {
        List<EventType> eventTypeList = getListOfAllEventTypes();

        int index = 0;
        for (EventType type : eventTypeList) {
            if (eventType.equals(type)) {
                return index;
            }
            index++;
        }

        return -1;
    }

    public static List<EventVisibility> getListOfAllEventVisibilities() {
        List<EventVisibility> eventVisibilityList = new ArrayList<>();
        eventVisibilityList.add(EventVisibility.PUBLIC);
        eventVisibilityList.add(EventVisibility.PRIVATE);

        return eventVisibilityList;
    }

    public static int getVisibilityIndexFromAllEventVisibilities(EventVisibility eventVisibility) {
        List<EventVisibility> eventVisibilityList = getListOfAllEventVisibilities();

        int index = 0;
        for (EventVisibility visibility : eventVisibilityList) {
            if (eventVisibility.equals(visibility)) {
                return index;
            }
            index++;
        }

        return -1;
    }

    public static String getJsonFromEventCreate(EventCreate eventCreate) {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            return objectMapper.writeValueAsString(eventCreate);
        } catch (JsonProcessingException e) {
            Log.e(LOG_TAG, "Error while creating json string from eventCreate object: " + e);
        }

        return null;
    }

    public static EventCreate getEventCreateObjectFromJson(String jsonString) {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            return objectMapper.readValue(jsonString, EventCreate.class);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error while creating eventCreate object from json string: " + e);
        }

        return null;
    }
}
