package sk.stuba.fei.stumee.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import sk.stuba.fei.stumee.R;
import sk.stuba.fei.stumee.model.event.EventType;
import sk.stuba.fei.stumee.utils.EventUtils;

public class CustomEventTypeSpinnerAdapter extends ArrayAdapter<EventType> {

    @SuppressWarnings("SameParameterValue")
    public CustomEventTypeSpinnerAdapter(Context context, int resource, List<EventType> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, parent);
    }

    private View getCustomView(int position, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customSpinnerView = inflater.inflate(R.layout.custom_spinner_layout, parent, false);

        TextView spinnerText = (TextView) customSpinnerView.findViewById(R.id.custom_spinner_text);
        ImageView spinnerIcon = (ImageView) customSpinnerView.findViewById(R.id.custom_spinner_icon);

        spinnerText.setText(EventUtils.getEventTypeTextId(getItem(position)));
        spinnerIcon.setImageResource(EventUtils.getEventTypeIcon(getItem(position)));

        return customSpinnerView;
    }
}
