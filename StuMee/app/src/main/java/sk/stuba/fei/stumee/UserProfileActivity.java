package sk.stuba.fei.stumee;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;

import sk.stuba.fei.stumee.asyncTasks.AbstractApiAsyncTask;
import sk.stuba.fei.stumee.model.Link;
import sk.stuba.fei.stumee.model.user.User;
import sk.stuba.fei.stumee.utils.AuthUtils;
import sk.stuba.fei.stumee.utils.UserUtils;
import sk.stuba.fei.stumee.utils.linking.LinkTypeEventUser;
import sk.stuba.fei.stumee.utils.linking.LinkUtils;

public class UserProfileActivity extends AppCompatActivity {
    private static final String LOG_TAG = "UserProfileActivity";

    private CoordinatorLayout baseCoordinatorLayout;
    private View userProfileFormView;
    private ProgressBar progressBar;
    private ImageView photoImageView;
    private EditText nicknameEditText;
    private EditText aisEmailEditText;
    private EditText secondaryEmailEditText;
    private EditText nameEditText;
    private EditText surnameEditText;
    private EditText descriptionEditText;

    private String userUpdateUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.user_profile_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        baseCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.user_profile_base_coordinator_layout);
        userProfileFormView = findViewById(R.id.user_profile_form_layout);
        progressBar = (ProgressBar) findViewById(R.id.user_profile_progress_bar);
        photoImageView = (ImageView) findViewById(R.id.user_profile_photo);
        nicknameEditText = (EditText) findViewById(R.id.user_profile_nickname);
        aisEmailEditText = (EditText) findViewById(R.id.user_profile_ais_email);
        secondaryEmailEditText = (EditText) findViewById(R.id.user_profile_secondary_email);
        nameEditText = (EditText) findViewById(R.id.user_profile_name);
        surnameEditText = (EditText) findViewById(R.id.user_profile_surname);
        descriptionEditText = (EditText) findViewById(R.id.user_profile_description);

        Button submitButton = (Button) findViewById(R.id.user_profile_submit_button);
        if (submitButton != null) {
            submitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    attemptUserProfileUpdate();
                }
            });
        }

        loadUserDetails();
        loadUserPhoto();
        showProgress(false);
    }

    private void loadUserDetails() {
        User user = UserUtils.loadUserObjectFromPrefs(getApplicationContext());
        if (user == null) {
            Log.e(LOG_TAG, "Loaded user object from shared preferences is null.");
            return;
        }

        nicknameEditText.setText(user.getUsername());
        aisEmailEditText.setText(user.getStuEmail());
        secondaryEmailEditText.setText(user.getSecondaryEmail());
        nameEditText.setText(user.getName());
        surnameEditText.setText(user.getSurname());
        descriptionEditText.setText(user.getDescription());

        userUpdateUrl = LinkUtils.getLinkHref(user, LinkTypeEventUser.SELF);
    }

    private void loadUserPhoto() {
        Bitmap photoBitmap = UserUtils.loadUserPictureFromFile(getApplicationContext());
        if (photoBitmap == null) {
            photoImageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.user_avatar));
        } else {
            photoImageView.setImageBitmap(photoBitmap);
        }
    }

    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            userProfileFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            userProfileFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    userProfileFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            progressBar.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            userProfileFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private void attemptUserProfileUpdate() {
        String nickname = nicknameEditText.getText().toString().trim();
        String secondaryEmail = secondaryEmailEditText.getText().toString().trim();
        String name = nameEditText.getText().toString().trim();
        String surname = surnameEditText.getText().toString().trim();
        String description = descriptionEditText.getText().toString().trim();
        String aisMail = aisEmailEditText.getText().toString().trim();

        nicknameEditText.setError(null);
        nameEditText.setError(null);
        surnameEditText.setError(null);
        secondaryEmailEditText.setError(null);

        View focusView = null;
        boolean isError = false;

        if (nickname.isEmpty()) {
            nicknameEditText.setError("Nickname can not be empty!");
            isError = true;
            focusView = nicknameEditText;
        }

        if (name.isEmpty()) {
            nameEditText.setError("Name can not be empty!");
            isError = true;
            focusView = nameEditText;
        }

        if (surname.isEmpty()) {
            surnameEditText.setError("Surname can not be empty!");
            isError = true;
            focusView = surnameEditText;
        }

        if (!secondaryEmail.isEmpty() && !UserUtils.isEmailValid(secondaryEmail)) {
            secondaryEmailEditText.setError("Secondary email is not valid!");
            isError = true;
            focusView = secondaryEmailEditText;
        }

        if (isError) {
            focusView.requestFocus();
        } else {
            User user = new User();
            user.setUsername(nickname);
            user.setSecondaryEmail(secondaryEmail.isEmpty() ? null : secondaryEmail);
            user.setName(name);
            user.setSurname(surname);
            user.setDescription(description);
            user.setStuEmail(aisMail);
            user.setConfirmed(true);
            user.setDeactivated(false);
            user.setLinks(new ArrayList<Link>());

            new UpdateUserProfileAsyncTask(user).execute();
        }

    }

    private class UpdateUserProfileAsyncTask extends AbstractApiAsyncTask<Void, Void, User> {
        private final User userUpdate;

        private UpdateUserProfileAsyncTask(User userUpdate) {
            super(AuthUtils.getAccessTokenFromPrefs(getApplicationContext()));
            this.userUpdate = userUpdate;
        }

        @Override
        protected void onPreExecute() {
            showProgress(true);
        }

        @Override
        protected User doInBackground(Void... voids) {
            try {
                final String url = userUpdateUrl;
                HttpEntity<User> request = new HttpEntity<>(userUpdate, header);
                ResponseEntity<User> result = restTemplate.exchange(url, HttpMethod.PATCH, request, User.class);

                if (result.getStatusCode().is2xxSuccessful()) return result.getBody();
            } catch (Exception e) {
                if (e instanceof HttpClientErrorException) {
                    Log.e(LOG_TAG, "Error while doing request -> " + ((HttpClientErrorException) e).getResponseBodyAsString());
                }
                Log.e(LOG_TAG, "Error while doing request -> " + e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(User user) {
            if (user != null) {
                UserUtils.saveUserObjectToPrefs(getApplicationContext(), user);
                Snackbar.make(baseCoordinatorLayout, "Profile updated!", Snackbar.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Error occurred while updating profile info! Please, try again!", Toast.LENGTH_SHORT).show();
            }

            showProgress(false);
        }
    }
}
