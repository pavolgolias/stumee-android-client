package sk.stuba.fei.stumee.asyncTasks;

import android.os.AsyncTask;

import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public abstract class AbstractApiAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {
    private static final int CONNECTION_TIMEOUT = 1000 * 25;
    private static final String AUTH_PARAM = "Authorization";
    private static final String BEARER_PARAM = "Bearer ";
    protected final MultiValueMap<String, String> header;
    protected final RestTemplate restTemplate;
    protected final String accessToken;

    protected AbstractApiAsyncTask(final String accessToken) {
        this.header = new LinkedMultiValueMap<>();
        this.header.add(AUTH_PARAM, BEARER_PARAM + accessToken);
        this.restTemplate = new RestTemplate();
        this.accessToken = accessToken;
        ((SimpleClientHttpRequestFactory) this.restTemplate.getRequestFactory()).setReadTimeout(CONNECTION_TIMEOUT);
        ((SimpleClientHttpRequestFactory) this.restTemplate.getRequestFactory()).setConnectTimeout(CONNECTION_TIMEOUT);
    }
}
