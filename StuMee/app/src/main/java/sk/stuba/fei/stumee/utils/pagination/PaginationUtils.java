package sk.stuba.fei.stumee.utils.pagination;

import android.net.Uri;

public class PaginationUtils {
    public static final String MAXIMUM_PAGE_SIZE = "2000";
    private static final String PAGE_PARAM_NAME_NUMBER = "page";
    private static final String PAGE_PARAM_NAME_SIZE = "size";
    private static final String PAGE_PARAM_NAME_SORT = "sort";
    private static final String PAGE_PARAM_NAME_SORT_ASC = "asc";
    private static final String PAGE_PARAM_NAME_SORT_DESC = "desc";

    public static String createPagedUrlWithNumberOrSize(String baseUrl, PaginationParam param, String paramValue) {
        if (baseUrl == null || param == null || paramValue == null)
            return null;

        Uri.Builder uriBuilder = Uri.parse(baseUrl).buildUpon();
        switch (param) {
            case PAGE:
                uriBuilder.appendQueryParameter(PAGE_PARAM_NAME_NUMBER, paramValue);
                break;
            case SIZE:
                uriBuilder.appendQueryParameter(PAGE_PARAM_NAME_SIZE, paramValue);
                break;
        }

        return uriBuilder.build().toString();
    }

    public static String createPagedUrlWithSort(String baseUrl, PaginationSortType sortType, String sortField) {
        if (baseUrl == null || sortType == null || sortField == null)
            return null;

        Uri.Builder uriBuilder = Uri.parse(baseUrl).buildUpon();
        switch (sortType) {
            case SORT_TYPE_ACS:
                uriBuilder.appendQueryParameter(PAGE_PARAM_NAME_SORT, sortField + "," + PAGE_PARAM_NAME_SORT_ASC);
                break;
            case SORT_TYPE_DESC:
                uriBuilder.appendQueryParameter(PAGE_PARAM_NAME_SORT, sortField + "," + PAGE_PARAM_NAME_SORT_DESC);
                break;
        }

        return uriBuilder.build().toString().replace("%2C", ",");
    }

    public static String createPagedUrlWithSortAndSize(String baseUrl, String pageSize, PaginationSortType sortType, String sortField) {
        if (baseUrl == null || pageSize == null || sortType == null || sortField == null)
            return null;

        Uri.Builder uriBuilder = Uri.parse(baseUrl).buildUpon();
        uriBuilder.appendQueryParameter(PAGE_PARAM_NAME_SIZE, pageSize);

        return createPagedUrlWithSort(uriBuilder.build().toString(), sortType, sortField);
    }
}
