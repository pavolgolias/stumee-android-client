package sk.stuba.fei.stumee.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchRequestObject {
    private String keyword;

    public SearchRequestObject() {
    }

    public SearchRequestObject(String keyword) {
        this.keyword = keyword;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    @Override
    public String toString() {
        return "SearchRequestObject{" +
                "keyword='" + keyword + '\'' +
                '}';
    }
}
