package sk.stuba.fei.stumee.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class ConnectionUtils {
    @SuppressWarnings("SameParameterValue")
    public static boolean isDeviceConnected(Context context, boolean showErrorMessage) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        boolean isDeviceConnected = networkInfo != null && networkInfo.isConnected();
        if (!isDeviceConnected && showErrorMessage) {
            Toast.makeText(context, "Please, check your internet connection!", Toast.LENGTH_SHORT).show();
        }

        return isDeviceConnected;
    }
}
