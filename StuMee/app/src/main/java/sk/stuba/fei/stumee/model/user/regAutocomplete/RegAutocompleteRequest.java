package sk.stuba.fei.stumee.model.user.regAutocomplete;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegAutocompleteRequest {
    private String suggestKey;
    private int maxItems;

    public RegAutocompleteRequest() {
    }

    public RegAutocompleteRequest(String suggestKey, int maxItems) {
        this.suggestKey = suggestKey;
        this.maxItems = maxItems;
    }

    public String getSuggestKey() {
        return suggestKey;
    }

    public void setSuggestKey(String suggestKey) {
        this.suggestKey = suggestKey;
    }

    public int getMaxItems() {
        return maxItems;
    }

    public void setMaxItems(int maxItems) {
        this.maxItems = maxItems;
    }

    @Override
    public String toString() {
        return "RegAutocompleteRequest{" +
                "suggestKey='" + suggestKey + '\'' +
                ", maxItems=" + maxItems +
                '}';
    }
}
