package sk.stuba.fei.stumee.utils;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Calendar;

public class AuthUtils {
    private static final String STUMEE_NATIVE_CLIENT_RESOURCE_ID = "stumee-native-client";
    private static final String STUMEE_NATIVE_CLIENT_SECRET = "u2DJ27kwvRgl6qkb";
    private static final String LOG_TAG = "AuthUtils";

    private static final String PARAM_GRAND_TYPE = "grant_type";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_REFRESH_TOKEN = "refresh_token";
    private static final String PARAM_USERNAME = "username";
    private static final String PARAM_AUTHORIZATION = "Authorization";
    private static final String PARAM_BASIC = "Basic ";

    public static MultiValueMap<String, String> createHttpClientHeader() {
        MultiValueMap<String, String> header = new LinkedMultiValueMap<>();

        String auth = STUMEE_NATIVE_CLIENT_RESOURCE_ID + ":" + STUMEE_NATIVE_CLIENT_SECRET;
        String credentialsBase64 = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
        header.add(PARAM_AUTHORIZATION, PARAM_BASIC + credentialsBase64);

        return header;
    }

    public static MultiValueMap<String, String> createFormDataForLogin(String username, String email) {
        MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();

        formData.add(PARAM_GRAND_TYPE, PARAM_PASSWORD);
        formData.add(PARAM_USERNAME, username);
        formData.add(PARAM_PASSWORD, email);

        return formData;
    }

    public static MultiValueMap<String, String> createFormDataForTokenRefresh(String refreshToken) {
        MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();

        formData.add(PARAM_GRAND_TYPE, PARAM_REFRESH_TOKEN);
        formData.add(PARAM_REFRESH_TOKEN, refreshToken);

        return formData;
    }

    public static void saveAuthCredentialsToPrefs(Context context, String accessToken, String refreshToken, Long expirationTime) {
        Calendar calendar = Calendar.getInstance();

        SharedPreferences.Editor editor = context.getSharedPreferences(Constants.TOKEN_GROUP_PREFS_KEY, 0).edit();
        editor.putString(Constants.ACCESS_TOKEN_PREFS_KEY, accessToken);
        editor.putString(Constants.REFRESH_TOKEN_PREFS_KEY, refreshToken);
        editor.putLong(Constants.EXPIRATION_TOKEN_PREFS_KEY, (expirationTime * 1000) + calendar.getTimeInMillis());
        editor.apply();
    }

    public static void clearAuthCredentialsFromPrefs(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(Constants.TOKEN_GROUP_PREFS_KEY, 0).edit();
        editor.remove(Constants.ACCESS_TOKEN_PREFS_KEY).remove(Constants.REFRESH_TOKEN_PREFS_KEY)
                .remove(Constants.EXPIRATION_TOKEN_PREFS_KEY).apply();
    }

    public static String getAccessTokenFromPrefs(Context context) {
        try {
            SharedPreferences preferences = context.getSharedPreferences(Constants.TOKEN_GROUP_PREFS_KEY, 0);
            return preferences.getString(Constants.ACCESS_TOKEN_PREFS_KEY, "");
        } catch (Exception e) {
            Log.e(LOG_TAG, "Error while loading access token from shared preferences!" + e.toString());
        }

        return "";
    }
}
