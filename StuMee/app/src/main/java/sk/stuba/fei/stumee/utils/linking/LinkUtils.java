package sk.stuba.fei.stumee.utils.linking;


import android.support.annotation.NonNull;

import sk.stuba.fei.stumee.model.Link;
import sk.stuba.fei.stumee.model.event.Event;
import sk.stuba.fei.stumee.model.user.User;

public class LinkUtils {
    public static String getCommonLinkByType(@NonNull LinkTypeCommon linkTypeCommon) {
        switch (linkTypeCommon) {
            case SERVER_BASE_URL:
                return LinkTypeCommon.SERVER_BASE_URL.getLinkUrl();
            case OAUTH_TOKEN_ENDPOINT:
                return LinkTypeCommon.SERVER_BASE_URL.getLinkUrl() + LinkTypeCommon.OAUTH_TOKEN_ENDPOINT.getLinkUrl();
            case OAUTH_REVOKE_TOKEN_ENDPOINT:
                return LinkTypeCommon.SERVER_BASE_URL.getLinkUrl() + LinkTypeCommon.OAUTH_REVOKE_TOKEN_ENDPOINT.getLinkUrl();
            case OAUTH_AIS_AUTOCOMPLETE:
                return LinkTypeCommon.SERVER_BASE_API_URL.getLinkUrl() + LinkTypeCommon.OAUTH_AIS_AUTOCOMPLETE.getLinkUrl();
            case SERVER_BASE_API_URL:
                return LinkTypeCommon.SERVER_BASE_API_URL.getLinkUrl();
            case USERS:
                return LinkTypeCommon.SERVER_BASE_API_URL.getLinkUrl() + LinkTypeCommon.USERS.getLinkUrl();
            case USERS_ME:
                return LinkTypeCommon.SERVER_BASE_API_URL.getLinkUrl() + LinkTypeCommon.USERS_ME.getLinkUrl();
            case USERS_SEARCH:
                return LinkTypeCommon.SERVER_BASE_API_URL.getLinkUrl() + LinkTypeCommon.USERS_SEARCH.getLinkUrl();
            case EVENTS:
                return LinkTypeCommon.SERVER_BASE_API_URL.getLinkUrl() + LinkTypeCommon.EVENTS.getLinkUrl();
            case EVENTS_CONFIRMED:
                return LinkTypeCommon.SERVER_BASE_API_URL.getLinkUrl() + LinkTypeCommon.EVENTS_CONFIRMED.getLinkUrl();
            case EVENTS_UNCONFIRMED:
                return LinkTypeCommon.SERVER_BASE_API_URL.getLinkUrl() + LinkTypeCommon.EVENTS_UNCONFIRMED.getLinkUrl();
            case EVENTS_OWN:
                return LinkTypeCommon.SERVER_BASE_API_URL.getLinkUrl() + LinkTypeCommon.EVENTS_OWN.getLinkUrl();
            case EVENTS_DECLINED:
                return LinkTypeCommon.SERVER_BASE_API_URL.getLinkUrl() + LinkTypeCommon.EVENTS_DECLINED.getLinkUrl();
            case EVENTS_OLD:
                return LinkTypeCommon.SERVER_BASE_API_URL.getLinkUrl() + LinkTypeCommon.EVENTS_OLD.getLinkUrl();
            case EVENTS_SEARCH:
                return LinkTypeCommon.SERVER_BASE_API_URL.getLinkUrl() + LinkTypeCommon.EVENTS_SEARCH.getLinkUrl();
            default:
                return null;
        }
    }

    public static String getLinkHref(Event event, @NonNull LinkTypeEventUser linkTypeEventUser) {
        final String relValue = linkTypeEventUser.getRelName();

        for (Link link : event.getLinks()) {
            if (link.getRel().equals(relValue)) {
                return link.getHref();
            }
        }

        return null;
    }

    public static String getLinkHref(User user, @NonNull LinkTypeEventUser linkTypeEventUser) {
        final String relValue = linkTypeEventUser.getRelName();

        for (Link link : user.getLinks()) {
            if (link.getRel().equals(relValue)) {
                return link.getHref();
            }
        }

        return null;
    }
}
