package sk.stuba.fei.stumee;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.tokenautocomplete.FilteredArrayAdapter;
import com.tokenautocomplete.TokenCompleteTextView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import sk.stuba.fei.stumee.adapters.CustomEventTypeSpinnerAdapter;
import sk.stuba.fei.stumee.adapters.CustomEventVisibilitySpinnerAdapter;
import sk.stuba.fei.stumee.asyncTasks.AbstractApiAsyncTask;
import sk.stuba.fei.stumee.model.contents.UsersContent;
import sk.stuba.fei.stumee.model.event.Event;
import sk.stuba.fei.stumee.model.event.EventCreate;
import sk.stuba.fei.stumee.model.event.EventType;
import sk.stuba.fei.stumee.model.event.EventVisibility;
import sk.stuba.fei.stumee.model.user.User;
import sk.stuba.fei.stumee.utils.AuthUtils;
import sk.stuba.fei.stumee.utils.Constants;
import sk.stuba.fei.stumee.utils.EventUtils;
import sk.stuba.fei.stumee.utils.UserUtils;
import sk.stuba.fei.stumee.utils.linking.LinkTypeCommon;
import sk.stuba.fei.stumee.utils.linking.LinkTypeEventUser;
import sk.stuba.fei.stumee.utils.linking.LinkUtils;
import sk.stuba.fei.stumee.utils.pagination.PaginationParam;
import sk.stuba.fei.stumee.utils.pagination.PaginationUtils;
import sk.stuba.fei.stumee.views.UserTokenAutocompleteView;

public class CreateEditEventActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {
    public static final String EDIT_EVENT_JSON = "stumee.eventJson";
    public static final String EDIT_EVENT_ID = "stumee.eventId";
    private static final String LOG_TAG = "CreateEditEventActivity";

    private UserTokenAutocompleteView userTokenAutocompleteView;
    private ProgressDialog progressDialog;
    private CoordinatorLayout baseCoordinatorLayout;
    private EditText titleEditText;
    private EditText descriptionEditText;
    private EditText locationEditText;
    private Button dateTimeButton;
    private Spinner eventTypeSpinner;
    private Spinner eventVisibilitySpinner;
    private Button submitButton;

    private Calendar selectedDateTime;
    private boolean wasTimeSelected = false;
    private List<User> selectedUsers = new ArrayList<>();

    // EDIT MODE
    private boolean isEditMode = false;
    private int editEventId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_edit_event);

        Toolbar toolbar = (Toolbar) findViewById(R.id.event_create_toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        setupView();

        if (getIntent().getExtras() != null) {
            EventCreate eventCreate = EventUtils.getEventCreateObjectFromJson(getIntent().getExtras().getString(EDIT_EVENT_JSON));
            prepareForEdit(eventCreate);

            isEditMode = true;
            editEventId = getIntent().getExtras().getInt(EDIT_EVENT_ID);
        }

        userTokenAutocompleteView = (UserTokenAutocompleteView) findViewById(R.id.event_create_token_autoComplete);
        if (savedInstanceState == null && userTokenAutocompleteView != null) {
            if (isEditMode) {
                userTokenAutocompleteView.setPrefix(getString(R.string.event_detail_view_autocomplete_text_edit));
            } else {
                userTokenAutocompleteView.setPrefix(getString(R.string.event_detail_view_autocomplete_text));
            }
        }

        new LoadUsersAsyncTask().execute();
    }

    private void setupView() {
        baseCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.activity_create_edit_event_coordinator);
        titleEditText = (EditText) findViewById(R.id.event_create_text_title);
        descriptionEditText = (EditText) findViewById(R.id.event_create_text_description);
        locationEditText = (EditText) findViewById(R.id.event_create_text_location);
        dateTimeButton = (Button) findViewById(R.id.event_create_button_dateTime);
        submitButton = (Button) findViewById(R.id.event_create_button_submit);

        eventTypeSpinner = (Spinner) findViewById(R.id.event_create_spinner_type);
        eventTypeSpinner.setAdapter(new CustomEventTypeSpinnerAdapter(getApplicationContext(), R.layout.custom_spinner_layout, EventUtils.getListOfAllEventTypes()));

        eventVisibilitySpinner = (Spinner) findViewById(R.id.event_create_spinner_visibility);
        eventVisibilitySpinner.setAdapter(new CustomEventVisibilitySpinnerAdapter(getApplicationContext(), R.layout.custom_spinner_layout, EventUtils.getListOfAllEventVisibilities()));

        dateTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateDialog();
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performEventCreation();
            }
        });
    }

    private void prepareForEdit(EventCreate eventCreate) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.create_event_edit_title);
        }
        submitButton.setText(R.string.create_event_edit_title);

        // texts
        titleEditText.setText(eventCreate.getTitle());
        descriptionEditText.setText(eventCreate.getDescription());
        locationEditText.setText(eventCreate.getLocation());

        // time and date
        selectedDateTime = Calendar.getInstance();
        selectedDateTime.setTime(eventCreate.getTimestamp());
        dateTimeButton.setText(Constants.DATE_AND_TIME_FORMAT.format(selectedDateTime.getTime()));
        wasTimeSelected = true;

        // set spinners
        eventTypeSpinner.setSelection(EventUtils.getTypeIndexFromAllEventTypes(eventCreate.getType()));
        eventVisibilitySpinner.setSelection(EventUtils.getVisibilityIndexFromAllEventVisibilities(eventCreate.getVisibility()));
    }

    private void showDateDialog() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dateDialog = DatePickerDialog
                .newInstance(CreateEditEventActivity.this, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));

        dateDialog.setMinDate(now);
        dateDialog.dismissOnPause(true);
        dateDialog.show(getFragmentManager(), "DatePickerDialog");
    }

    private void showTimeDialog() {
        Calendar now = Calendar.getInstance();
        TimePickerDialog dateDialog = TimePickerDialog
                .newInstance(CreateEditEventActivity.this, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), true);

        dateDialog.dismissOnPause(true);
        dateDialog.show(getFragmentManager(), "TimePickerDialog");
    }

    private void setupUserAutocomplete(List<User> users) {
        // remove current user
        User currentUser = UserUtils.loadUserObjectFromPrefs(getApplicationContext());
        if (currentUser != null) {
            users.remove(currentUser);
        }

        ArrayAdapter<User> adapter = new FilteredArrayAdapter<User>(this, R.layout.token_autocomplete_item_layout, users) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null) {
                    LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                    convertView = layoutInflater.inflate(R.layout.token_autocomplete_item_layout, parent, false);
                }

                User user = getItem(position);
                String userName = user.getName() + " " + user.getSurname();
                String userId = user.getUsername() + " (" + String.valueOf(user.getId()) + ")";
                ((TextView) convertView.findViewById(R.id.token_autocomplete_item_userName)).setText(userName);
                ((TextView) convertView.findViewById(R.id.token_autocomplete_item_userId)).setText(userId);

                return convertView;
            }

            @Override
            protected boolean keepObject(User user, String mask) {
                mask = mask.toLowerCase();
                String userName = user.getName() + " " + user.getSurname();
                String userId = user.getUsername() + "(" + String.valueOf(user.getId()) + ")";

                return userName.toLowerCase().contains(mask) || userId.toLowerCase().contains(mask);
            }
        };

        userTokenAutocompleteView.setAdapter(adapter);
        userTokenAutocompleteView.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Select);
        userTokenAutocompleteView.allowDuplicates(false);
        userTokenAutocompleteView.allowCollapse(true);
        userTokenAutocompleteView.performBestGuess(true);
        userTokenAutocompleteView.setTokenListener(new TokenCompleteTextView.TokenListener<User>() {
            @Override
            public void onTokenAdded(User token) {
                selectedUsers.add(token);
            }

            @Override
            public void onTokenRemoved(User token) {
                selectedUsers.remove(token);
            }
        });
    }

    private void performEventCreation() {
        if (areEventCreationInputsValid()) {
            new CreateEditEventAsyncTask(createEventFromInputs()).execute();
        }
    }

    private void performUsersInvitation(Event event) {
        if (!selectedUsers.isEmpty()) {
            List<Integer> selectedIds = new ArrayList<>();

            for (User user : selectedUsers) {
                selectedIds.add(user.getId());
            }
            new PostInvitedToEventAsyncTask(event, selectedIds).execute();
        } else {
            hideProgressDialog();
        }
    }

    private EventCreate createEventFromInputs() {
        EventCreate event = new EventCreate();
        event.setTitle(titleEditText.getText().toString().trim());
        event.setDescription(descriptionEditText.getText().toString().trim());
        event.setTimestamp(selectedDateTime.getTime());
        event.setType((EventType) eventTypeSpinner.getSelectedItem());
        event.setVisibility((EventVisibility) eventVisibilitySpinner.getSelectedItem());
        event.setLocation(locationEditText.getText().toString().trim());

        return event;
    }

    private boolean areEventCreationInputsValid() {
        boolean isValid = true;
        View focusView = null;

        // reset errors
        titleEditText.setError(null);
        descriptionEditText.setError(null);
        locationEditText.setError(null);

        if (titleEditText.getText().toString().trim().isEmpty()) {
            isValid = false;
            focusView = titleEditText;
            titleEditText.setError("Event title can not be empty!");
        }

        if (descriptionEditText.getText().toString().trim().isEmpty()) {
            isValid = false;
            focusView = descriptionEditText;
            descriptionEditText.setError("Event description can not be empty!");
        }

        if (locationEditText.getText().toString().trim().isEmpty()) {
            isValid = false;
            focusView = locationEditText;
            locationEditText.setError("Event location can not be empty!");
        }

        if (!wasTimeSelected || selectedDateTime == null) {
            focusView = null;
            isValid = false;
            Snackbar.make(baseCoordinatorLayout, "Date or time was not set!", Snackbar.LENGTH_LONG)
                    .setAction("Set now", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showDateDialog();
                        }
                    })
                    .show();
        } else if (selectedDateTime.before(Calendar.getInstance())) {
            focusView = null;
            isValid = false;
            Snackbar.make(baseCoordinatorLayout, "Date of event has to be set to future!", Snackbar.LENGTH_LONG)
                    .setAction("Set now", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showDateDialog();
                        }
                    })
                    .show();
        }

        if (!isValid && focusView != null) {
            focusView.requestFocus();
        }

        return isValid;
    }

    private void clearInputForm() {
        titleEditText.setText("");
        titleEditText.clearFocus();
        descriptionEditText.setText("");
        descriptionEditText.clearFocus();
        locationEditText.setText("");
        locationEditText.clearFocus();
        dateTimeButton.setText(R.string.create_event_date_button_text);
        wasTimeSelected = false;
    }

    private void showProgressDialog() {
        progressDialog = new ProgressDialog(CreateEditEventActivity.this);
        progressDialog.setTitle(getString(R.string.progress_dialog_title_loading));
        progressDialog.setMessage(getString(R.string.progress_dialog_text_createEdit));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
    }

    private void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        wasTimeSelected = false;
        selectedDateTime = Calendar.getInstance();
        selectedDateTime.set(year, monthOfYear, dayOfMonth);
        showTimeDialog();
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        wasTimeSelected = true;
        selectedDateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
        selectedDateTime.set(Calendar.MINUTE, minute);
        selectedDateTime.set(Calendar.SECOND, second);
        dateTimeButton.setText(Constants.DATE_AND_TIME_FORMAT.format(selectedDateTime.getTime()));
    }

    private class CreateEditEventAsyncTask extends AbstractApiAsyncTask<Void, Void, Event> {
        private final EventCreate eventCreate;

        private CreateEditEventAsyncTask(EventCreate eventCreate) {
            super(AuthUtils.getAccessTokenFromPrefs(getApplicationContext()));
            this.eventCreate = eventCreate;
        }

        @Override
        protected void onPreExecute() {
            showProgressDialog();
        }

        @Override
        protected Event doInBackground(Void... voids) {
            try {
                HttpEntity<EventCreate> request = new HttpEntity<>(eventCreate, header);

                HttpMethod httpMethod;
                final String url;
                if (isEditMode) {
                    url = LinkUtils.getCommonLinkByType(LinkTypeCommon.EVENTS) + "/" + editEventId;
                    httpMethod = HttpMethod.PATCH;
                } else {
                    url = LinkUtils.getCommonLinkByType(LinkTypeCommon.EVENTS);
                    httpMethod = HttpMethod.POST;
                }

                ResponseEntity<Event> result = restTemplate.exchange(url, httpMethod, request, Event.class);
                if (result.getStatusCode().is2xxSuccessful()) return result.getBody();
            } catch (Exception e) {
                if (e instanceof HttpClientErrorException) {
                    Log.e(LOG_TAG, "Error while doing request -> " + ((HttpClientErrorException) e).getResponseBodyAsString());
                }
                Log.e(LOG_TAG, "Error while doing request -> " + e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Event event) {
            if (event != null) {
                performUsersInvitation(event);
                clearInputForm();
                Snackbar.make(baseCoordinatorLayout, "Event " + event.getTitle() + (isEditMode ? " was updated!" : " was created!"), Snackbar.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Error occurred while " + (isEditMode ? "updating" : "creating") + " event!", Toast.LENGTH_SHORT).show();
                hideProgressDialog();
            }
        }
    }

    private class LoadUsersAsyncTask extends AbstractApiAsyncTask<Void, Void, UsersContent> {
        LoadUsersAsyncTask() {
            super(AuthUtils.getAccessTokenFromPrefs(getApplicationContext()));
        }

        @Override
        protected UsersContent doInBackground(Void... voids) {
            try {
                String url;
                if (isEditMode) {
                    url = LinkUtils.getCommonLinkByType(LinkTypeCommon.EVENTS) + "/" + editEventId + "/" + LinkTypeEventUser.INVITABLE.getRelName();
                } else {
                    url = LinkUtils.getCommonLinkByType(LinkTypeCommon.USERS);
                }
                url = PaginationUtils.createPagedUrlWithNumberOrSize(url, PaginationParam.SIZE, PaginationUtils.MAXIMUM_PAGE_SIZE);

                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                HttpEntity<?> request = new HttpEntity<>(header);
                ResponseEntity<UsersContent> result = restTemplate.exchange(url, HttpMethod.GET, request, UsersContent.class);

                if (result.getStatusCode().is2xxSuccessful()) return result.getBody();
            } catch (Exception e) {
                if (e instanceof HttpClientErrorException) {
                    Log.e(LOG_TAG, "Error while doing request -> " + ((HttpClientErrorException) e).getResponseBodyAsString());
                }
                Log.e(LOG_TAG, "Error while doing request -> " + e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(UsersContent usersContent) {
            if (usersContent != null) {
                setupUserAutocomplete(usersContent.getContent());
            } else {
                Toast.makeText(getApplicationContext(), "Error occurred while loading users to invite!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class PostInvitedToEventAsyncTask extends AbstractApiAsyncTask<Void, Void, Boolean> {
        private final Event event;
        private final List<Integer> usersIds;

        private PostInvitedToEventAsyncTask(Event event, List<Integer> usersIds) {
            super(AuthUtils.getAccessTokenFromPrefs(getApplicationContext()));
            this.event = event;
            this.usersIds = usersIds;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                final String url = LinkUtils.getLinkHref(event, LinkTypeEventUser.EVENT_INVITED);
                if (url == null) {
                    return false;
                }

                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                HttpEntity<List<Integer>> request = new HttpEntity<>(usersIds, header);
                ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.POST, request, String.class);

                if (result.getStatusCode().is2xxSuccessful()) return true;
            } catch (Exception e) {
                if (e instanceof HttpClientErrorException) {
                    Log.e(LOG_TAG, "Error while doing request -> " + ((HttpClientErrorException) e).getResponseBodyAsString());
                }
                Log.e(LOG_TAG, "Error while doing request -> " + e.toString());
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean isSuccessful) {
            if (!isSuccessful) {
                Toast.makeText(getApplicationContext(), "Error occurred while inviting people to event!", Toast.LENGTH_SHORT).show();
            }

            userTokenAutocompleteView.clear();
            hideProgressDialog();
        }
    }
}
