package sk.stuba.fei.stumee.model.event;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EventCreate {
    private String title;
    private String description;
    private Date timestamp;
    private EventType type;
    private EventVisibility visibility;
    private String location;

    public EventCreate() {
    }

    public EventCreate(String title, String description, Date timestamp, EventType type, EventVisibility visibility, String location) {
        this.title = title;
        this.description = description;
        this.timestamp = timestamp;
        this.type = type;
        this.visibility = visibility;
        this.location = location;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public EventVisibility getVisibility() {
        return visibility;
    }

    public void setVisibility(EventVisibility visibility) {
        this.visibility = visibility;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "EventCreate{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", timestamp=" + timestamp +
                ", type=" + type +
                ", visibility=" + visibility +
                ", location='" + location + '\'' +
                '}';
    }
}
