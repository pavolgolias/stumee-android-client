package sk.stuba.fei.stumee.model.contents;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

import sk.stuba.fei.stumee.model.event.comments.Comment;
import sk.stuba.fei.stumee.model.Page;
import sk.stuba.fei.stumee.model.Link;

@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
public class EventCommentsContent {
    private List<Link> links;
    private List<Comment> content;
    private Page page;

    public EventCommentsContent() {
    }

    public EventCommentsContent(List<Link> links, List<Comment> content, Page page) {
        this.links = links;
        this.content = content;
        this.page = page;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    public List<Comment> getContent() {
        return content;
    }

    public void setContent(List<Comment> content) {
        this.content = content;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    @Override
    public String toString() {
        return "EventCommentsContent{" +
                "links=" + links +
                ", content=" + content +
                ", page=" + page +
                '}';
    }
}
