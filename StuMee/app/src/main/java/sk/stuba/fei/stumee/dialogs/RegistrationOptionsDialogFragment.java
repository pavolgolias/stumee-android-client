package sk.stuba.fei.stumee.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import java.util.List;

import sk.stuba.fei.stumee.R;
import sk.stuba.fei.stumee.model.user.UserCreate;
import sk.stuba.fei.stumee.model.user.regAutocomplete.RegAutocompleteResponseContent;
import sk.stuba.fei.stumee.utils.UserUtils;

public class RegistrationOptionsDialogFragment extends DialogFragment {
    private static final String LOG_TAG = "RegOptDialogFragment";
    private static final String DIALOG_ITEMS = "regDialog.items";
    private static final String DIALOG_USER_NAME = "regDialog.userName";
    private static final String DIALOG_EMAIL = "regDialog.email";
    private static final String DIALOG_PASSWORD = "regDialog.password";
    private static final int DEFAULT_SELECTED_ITEM = 0;

    private CharSequence selectedItem;
    private RegistrationOptionsDialogListener clickListener;

    public static RegistrationOptionsDialogFragment newInstance(List<RegAutocompleteResponseContent> contentList, String userName, String email, String password) {
        RegistrationOptionsDialogFragment dialog = new RegistrationOptionsDialogFragment();
        Bundle args = new Bundle();

        args.putCharSequenceArray(DIALOG_ITEMS, createPossibilities(contentList));
        args.putString(DIALOG_USER_NAME, userName);
        args.putString(DIALOG_EMAIL, email);
        args.putString(DIALOG_PASSWORD, password);
        dialog.setArguments(args);

        return dialog;
    }

    private static CharSequence[] createPossibilities(List<RegAutocompleteResponseContent> contentList) {
        CharSequence[] items = new CharSequence[contentList.size()];
        int index = 0;

        for (RegAutocompleteResponseContent item : contentList) {
            items[index] = item.getWholeName() + " (" + item.getId() + ")\n" + item.getDepartment();
            index++;
        }

        return items;
    }

    private String getWholeNameFromSelectedPossibility() {
        return selectedItem.toString().substring(0, selectedItem.toString().indexOf("(") - 1);
    }

    private int getIdFromSelectedPossibility() {
        String stringId = selectedItem.toString().substring(selectedItem.toString().indexOf("(") + 1, selectedItem.toString().indexOf(")"));
        try {
            return Integer.valueOf(stringId);
        } catch (NumberFormatException e) {
            Log.e(LOG_TAG, "Error while casting string to integer. " + e);
        }

        return 0;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final String userName = getArguments().getString(DIALOG_USER_NAME);
        final String email = getArguments().getString(DIALOG_EMAIL);
        final String password = getArguments().getString(DIALOG_PASSWORD);
        final CharSequence[] items = getArguments().getCharSequenceArray(DIALOG_ITEMS);
        if (items != null) {
            selectedItem = items[DEFAULT_SELECTED_ITEM];
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.reg_opt_dialog_title)
                .setIcon(R.mipmap.ic_launcher)
                .setSingleChoiceItems(items, DEFAULT_SELECTED_ITEM, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (items != null) {
                            selectedItem = items[i];
                        }
                    }
                })
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String name = UserUtils.getNameFromWholeAisName(getWholeNameFromSelectedPossibility());
                        String surname = UserUtils.getSurnameFromWholeAisName(getWholeNameFromSelectedPossibility());

                        UserCreate userCreate = new UserCreate(getIdFromSelectedPossibility(), userName, email, name, surname, password);
                        clickListener.onRegOptionsDialogPositiveClick(userCreate);
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton(R.string.reg_opt_dialog_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            clickListener = (RegistrationOptionsDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement RegistrationOptionsDialogListener!");
        }
    }

    public interface RegistrationOptionsDialogListener {
        void onRegOptionsDialogPositiveClick(UserCreate userCreate);
    }
}
