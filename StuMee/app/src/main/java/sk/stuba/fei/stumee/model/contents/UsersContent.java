package sk.stuba.fei.stumee.model.contents;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

import sk.stuba.fei.stumee.model.Link;
import sk.stuba.fei.stumee.model.Page;
import sk.stuba.fei.stumee.model.user.User;

@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
public class UsersContent {
    private List<Link> links;
    private List<User> content;
    private Page page;

    public UsersContent() {
    }

    public UsersContent(List<Link> links, List<User> content, Page page) {
        this.links = links;
        this.content = content;
        this.page = page;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    public List<User> getContent() {
        return content;
    }

    public void setContent(List<User> content) {
        this.content = content;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    @Override
    public String toString() {
        return "UsersContent{" +
                "links=" + links +
                ", content=" + content +
                ", page=" + page +
                '}';
    }
}
