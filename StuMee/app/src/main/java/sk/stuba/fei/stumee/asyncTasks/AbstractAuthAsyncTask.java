package sk.stuba.fei.stumee.asyncTasks;

import android.os.AsyncTask;

import org.springframework.http.HttpEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import sk.stuba.fei.stumee.utils.AuthUtils;
import sk.stuba.fei.stumee.utils.linking.LinkTypeCommon;
import sk.stuba.fei.stumee.utils.linking.LinkUtils;

public abstract class AbstractAuthAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {
    private static final int CONNECTION_TIMEOUT = 1000 * 25;
    protected final String url;
    private final MultiValueMap<String, String> formData;
    protected RestTemplate restTemplate;
    protected HttpEntity<MultiValueMap<String, String>> requestEntity;

    public AbstractAuthAsyncTask(String username, String password) {
        this.url = LinkUtils.getCommonLinkByType(LinkTypeCommon.OAUTH_TOKEN_ENDPOINT);
        this.formData = AuthUtils.createFormDataForLogin(username, password);
        createCommonParts();
    }

    public AbstractAuthAsyncTask(String refreshToken) {
        this.url = LinkUtils.getCommonLinkByType(LinkTypeCommon.OAUTH_TOKEN_ENDPOINT);
        this.formData = AuthUtils.createFormDataForTokenRefresh(refreshToken);
        createCommonParts();
    }

    private void createCommonParts() {
        this.restTemplate = new RestTemplate();
        this.requestEntity = new HttpEntity<>(this.formData, AuthUtils.createHttpClientHeader());
        ((SimpleClientHttpRequestFactory) this.restTemplate.getRequestFactory()).setReadTimeout(CONNECTION_TIMEOUT);
        ((SimpleClientHttpRequestFactory) this.restTemplate.getRequestFactory()).setConnectTimeout(CONNECTION_TIMEOUT);
    }
}
