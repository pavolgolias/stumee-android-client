package sk.stuba.fei.stumee.utils.linking;

public enum LinkTypeEventUser {
    SELF("self"),

    // event
    EVENT_COMMENTS("comments"),
    EVENT_ATTACHMENTS("attachments"),
    EVENT_AUTHORS("authors"),
    EVENT_INVITED("invited"),
    EVENT_GOING("going"),
    EVENT_NOT_GOING("notGoing"),
    INVITABLE("invitable"),
    INVITABLE_SEARCH("invitable-search"),

    // user
    USER_PICTURE("picture"),
    USER_PICTURE_DATA("pictureData");

    private final String relName;

    LinkTypeEventUser(String relName) {
        this.relName = relName;
    }

    public String getRelName() {
        return relName;
    }
}
