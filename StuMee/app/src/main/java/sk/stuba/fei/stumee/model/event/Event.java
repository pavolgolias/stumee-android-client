package sk.stuba.fei.stumee.model.event;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;
import java.util.List;

import sk.stuba.fei.stumee.model.Link;

@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Event {
    private int id;
    private String title;
    private String description;
    private Date created;
    private Date timestamp;
    private EventType type;
    private EventVisibility visibility;
    private String location;
    private boolean canceled;
    private List<Link> links;

    public Event() {
    }

    public Event(int id, String title, String description, Date created, Date timestamp, EventType type, EventVisibility visibility, String location, boolean canceled, List<Link> links) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.created = created;
        this.timestamp = timestamp;
        this.type = type;
        this.visibility = visibility;
        this.location = location;
        this.canceled = canceled;
        this.links = links;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public EventVisibility getVisibility() {
        return visibility;
    }

    public void setVisibility(EventVisibility visibility) {
        this.visibility = visibility;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isCanceled() {
        return canceled;
    }

    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", created=" + created +
                ", timestamp=" + timestamp +
                ", type=" + type +
                ", visibility=" + visibility +
                ", location='" + location + '\'' +
                ", canceled=" + canceled +
                ", links=" + links +
                '}';
    }
}
