package sk.stuba.fei.stumee.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import sk.stuba.fei.stumee.R;

public class UserTokenAutocompleteLayout extends LinearLayout {
    public UserTokenAutocompleteLayout(Context context) {
        super(context);
    }

    public UserTokenAutocompleteLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public UserTokenAutocompleteLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);

        TextView v = (TextView) findViewById(R.id.token_auto_layout_text);
        if (selected) {
            v.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_close_black_24dp, 0);
        } else {
            v.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }
}
