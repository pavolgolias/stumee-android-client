package sk.stuba.fei.stumee.model.event;

public enum EventVisibility {
    PUBLIC,
    PRIVATE
}
