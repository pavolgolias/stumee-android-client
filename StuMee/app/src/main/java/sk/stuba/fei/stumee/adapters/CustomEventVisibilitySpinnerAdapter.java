package sk.stuba.fei.stumee.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import sk.stuba.fei.stumee.R;
import sk.stuba.fei.stumee.model.event.EventVisibility;
import sk.stuba.fei.stumee.utils.EventUtils;

public class CustomEventVisibilitySpinnerAdapter extends ArrayAdapter<EventVisibility> {

    @SuppressWarnings("SameParameterValue")
    public CustomEventVisibilitySpinnerAdapter(Context context, int resource, List<EventVisibility> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, parent);
    }

    private View getCustomView(int position, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customSpinnerView = inflater.inflate(R.layout.custom_spinner_layout, parent, false);

        TextView spinnerText = (TextView) customSpinnerView.findViewById(R.id.custom_spinner_text);
        ImageView spinnerIcon = (ImageView) customSpinnerView.findViewById(R.id.custom_spinner_icon);

        spinnerText.setText(EventUtils.getEventVisibilityTextId((getItem(position))));
        spinnerIcon.setImageResource(EventUtils.getEventVisibilityIcon(getItem(position)));

        return customSpinnerView;
    }
}
