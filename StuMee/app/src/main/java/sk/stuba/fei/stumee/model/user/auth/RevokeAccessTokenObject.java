package sk.stuba.fei.stumee.model.user.auth;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
public class RevokeAccessTokenObject {
    private String accessToken;

    public RevokeAccessTokenObject() {
    }

    public RevokeAccessTokenObject(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    @Override
    public String toString() {
        return "RevokeAccessTokenObject{" +
                "accessToken='" + accessToken + '\'' +
                '}';
    }
}
