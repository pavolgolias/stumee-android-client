package sk.stuba.fei.stumee;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

import sk.stuba.fei.stumee.asyncTasks.AbstractAuthAsyncTask;
import sk.stuba.fei.stumee.model.user.UserCredentials;
import sk.stuba.fei.stumee.utils.AuthUtils;
import sk.stuba.fei.stumee.utils.ConnectionUtils;
import sk.stuba.fei.stumee.utils.UserUtils;

public class LoginActivity extends AppCompatActivity {
    private static final String LOG_TAG = "LoginActivity";

    private ProgressBar progressBar;
    private View loginContentView;
    private EditText emailEditText;
    private EditText passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        progressBar = (ProgressBar) findViewById(R.id.home_login_progress_bar);
        loginContentView = findViewById(R.id.home_login_content);
        emailEditText = (EditText) findViewById(R.id.home_login_email);
        passwordEditText = (EditText) findViewById(R.id.home_login_password);

        Button loginSubmitButton = (Button) findViewById(R.id.home_login_submit_button);
        if (loginSubmitButton != null) {
            loginSubmitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ConnectionUtils.isDeviceConnected(getApplicationContext(), true)) {
                        attemptLogIn();
                    }
                }
            });
        }

        TextView registerText = (TextView) findViewById(R.id.home_register_text);
        if (registerText != null) {
            registerText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ConnectionUtils.isDeviceConnected(getApplicationContext(), true)) {
                        Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
                        startActivity(intent);
                    }
                }
            });
        }

        showProgress(false);

        // users tokens expired or new login is starting now - we don't want to store previous user data anymore
        UserUtils.removeUserObjectFromPrefs(getApplicationContext());
    }

    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            loginContentView.setVisibility(show ? View.GONE : View.VISIBLE);
            loginContentView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    loginContentView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            progressBar.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            loginContentView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private void attemptLogIn() {
        // Reset errors
        emailEditText.setError(null);
        passwordEditText.setError(null);

        boolean isError = false;
        View focusView = null;

        String email = emailEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString().trim();

        if (!UserUtils.isStuEmailValid(email)) {
            emailEditText.setError("Email is not correct! Use yourAisLogin@stuba.sk!");
            focusView = emailEditText;
            isError = true;
        }

        if (!UserUtils.isPasswordValid(password)) {
            passwordEditText.setError("Password is not correct!");
            focusView = passwordEditText;
            isError = true;
        }

        if (isError) {
            focusView.requestFocus();
        } else if (ConnectionUtils.isDeviceConnected(getApplicationContext(), true)) {
            new PerformLoginAsyncTask(email, password).execute();
        }
    }

    private class PerformLoginAsyncTask extends AbstractAuthAsyncTask<Void, Void, UserCredentials> {
        private PerformLoginAsyncTask(String username, String password) {
            super(username, password);
        }

        @Override
        protected void onPreExecute() {
            showProgress(true);
        }

        @Override
        protected UserCredentials doInBackground(Void... voids) {
            try {
                ResponseEntity<UserCredentials> result = restTemplate.exchange(url, HttpMethod.POST, requestEntity, UserCredentials.class);
                if (result.getStatusCode().is2xxSuccessful()) return result.getBody();
            } catch (Exception e) {
                if (e instanceof HttpClientErrorException) {
                    Log.e(LOG_TAG, "Error while doing request -> " + ((HttpClientErrorException) e).getResponseBodyAsString());
                }
                Log.e(LOG_TAG, "Error while doing request -> " + e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(UserCredentials userCredentials) {
            if (userCredentials != null) {
                AuthUtils.saveAuthCredentialsToPrefs(getApplicationContext(), userCredentials.getAccessToken(),
                        userCredentials.getRefreshToken(), userCredentials.getExpiresIn());

                Intent intent = new Intent(getApplicationContext(), EventActivity.class);
                startActivity(intent);
                finish();
            } else {
                showProgress(false);
                Toast.makeText(getApplicationContext(), "Error occurred while logging in! Please, try again or register first!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}